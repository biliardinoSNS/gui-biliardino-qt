/**
 * @file partita.h
 * @brief Classe Partita, utilizzata dalla MainWindow per memorizzare le informazioni attuali sui giocatori.
 */



#ifndef PARTITA_H
#define PARTITA_H

#include <map>
#include <iostream>

#include <jsoncpp/json/json.h>
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>

#include "giocatore.h"
#include "local_settings.h"
#include "utils.h"

class Partita
{
public:
    Partita();
    Partita(Giocatore, Giocatore, Giocatore, Giocatore, unsigned int, unsigned int);
    Partita(Json::Value); /**< Questo costruttore viene chiamato quando la partita viene ricostruita a partire dal json quando la GUI è stata chiusa senza terminarla. */
    enum class Collegio { Timpano, Fermi, Carducci, Faedo }; /**< Enum abbastanza evidente */
    enum class Squadra { Red, Blu }; /**< Enum ancora più evidente */
    enum class tipoEvento { GOAL_F, SUPERGOAL, GOAL_ANN, GOAL_B, SWAP }; /**< I tipi di evento che possono accadere in una partita, con le specifiche sui vari tipi di goal */

    class Posizione {
    public:
        std::string breve() const { std::string ret; if (ruolo == Giocatore::Ruolo::Attacco) ret += "A"; else ret += "D"; if (squadra == Squadra::Blu) ret += "B"; else ret += "R"; return ret; }
        std::string lungo() const;

        Posizione() { ruolo = Giocatore::Ruolo::Attacco; squadra = Squadra::Red; }
        Posizione(Giocatore::Ruolo r, Squadra s) { ruolo = r; squadra = s; }
        Posizione(Giocatore::Ruolo& r, Squadra& s) { ruolo = r; squadra = s; }

	/**
	 * Questo ordinamento è palesemente fuffa, ma non possono esistere std::map con tipi non ordinabili come keys, quindi ho dovuto metterlo.
	 */
        bool operator<(const Posizione& p) const { if (ruolo == p.ruolo) return squadra < p.squadra; else return ruolo < p.ruolo; }
        Giocatore::Ruolo get_ruolo() const { return ruolo; }
        Squadra get_squadra() const { return squadra; }

    private:
        Giocatore::Ruolo ruolo;
        Squadra squadra;
    };

    class EventoSquadra {
       public:
        EventoSquadra() { evento = tipoEvento::GOAL_ANN; squadra = Squadra::Blu; }
        EventoSquadra(tipoEvento t, Squadra s) { evento = t; squadra = s; }
        tipoEvento evento;
        Squadra squadra;
    };


    std::map<Posizione, Giocatore>& get_giocatori() { return giocatori; }
    std::map<Squadra, unsigned int> get_punteggi() const { return punteggi; }
    void set_pk(int p) { pk = p; } /**< Setta il valore della primary key della partita. */
    void set_punteggi(int b, int r) { punteggi[Squadra::Red] = r; punteggi[Squadra::Blu] = b; } /**< Autoesplicativo. */

    void edit_punteggi(Squadra s, int pt, bool coda); /**< Modifica di pt il punteggio della squadra s */
    unsigned int get_pk() const { return pk; }
    bool fine(); /**< Controlla se, secondo i punteggi attuali, la partita va finita o meno. */
    void annulla_vantaggi() { vantaggi = false; }
    bool controllaNessuno() const; /**< Controlla che nessuno dei giocatori sia Giocatore::nessuno */
    bool controllaPlayerUguali() const; /**< Controlla che non ci siano due giocatori uguali. */
    std::string vincitore() { if (punteggi[Squadra::Red] > punteggi[Squadra::Blu]) return "r"; else if (punteggi[Squadra::Red] < punteggi[Squadra::Blu]) return "b"; else return "p"; }

    friend std::ostream& operator<<(std::ostream& os, const Partita& p);

    static unsigned int GOAL_VITTORIA; /**< Goal necessari per vincere */
    static Collegio collegio;

private:
    std::map<Squadra, unsigned int> punteggi;
    std::map<Posizione, Giocatore> giocatori;
    int pk;
    bool vantaggi = false;
};

Partita::Collegio coll_fromStdStr(const std::string s);
std::string s_breve(const Partita::Squadra& s);
std::string col_breve(const Partita::Collegio c);
std::string col_lungo(const Partita::Collegio c);




#endif // PARTITA_H
