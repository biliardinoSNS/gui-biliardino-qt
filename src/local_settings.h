/**
 * @file local_settings.h
 * @brief File con metodi per caricare impostazioni da non hardcodare da dei file locali. Il default per la posizione del file è "/etc/biliardinoGUI/config.json"
 * Tutte le variabili dichiarate nel namespace LOCAL non hanno senso di essere hardcodate 
 * per cui vengono caricate da un config file. Il default della posizione del config è "/etc/biliardinoGUI/config.json".
 * Esiste una versione di esempio del file config nella home del progetto.
 * Chiaramente potrebbe essere scomoda questa posizione. Per modificarla è necessario settare la variabile d'ambiente
 * "GUI_BILIARDINO_CONFIG_PATH". Che si può ottenere per esempio in bash
 * $ export GUI_BILIARDINO_CONFIG_PATH=/home/cusu/biliardino/config.json
 * È possibile anche settarla come variabile dentro QtCreator. Si veda quella documentazione per ulteriori informazioni.
 */


#ifndef LOCAL_SETTINGS_H
#define LOCAL_SETTINGS_H

#include <string>
#include <fstream>
#include <iostream>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/value.h>

#include "utils.h"


/**
 * @brief Namespace con tutte le variabili locali da non hardcodare.
 */
namespace LOCAL {
    extern std::string BASE_URL_REQUEST; /**< Url a cui fare le richieste. */
    extern std::string TOKEN; /**< Token di sicurezza. */
    extern std::string COLLEGIO; /**< Collegio in cui si sta eseguendo la GUI. */
    extern std::string ROOT_SITE; /**< La '/' del web server. */
    extern std::string PERCORSO_FOTO; /**< Percorso nel filesystem dove salvare e recuperare le foto degli utenti. */
    extern std::string PERCORSO_AUDIO; /**< Percorso nel filesystem dove leggere i file audio da riprodurre. */
    extern int GOAL_VITTORIA; /**< Numero di goal segnati in assenza di vantaggi per terminare automaticamente la partita */
    extern bool supergoal; /**< Il biliardino ha i supergoal? */
    extern std::string username; /**< Username dell'utente del biliardino */
    extern std::string password; /**< Password dell'utente del biliardino */
    void load_settings(); /**< Funzione che carica dal file config.json le informazioni nelle variabili dichiarate in questo namespace */
}


#endif // LOCAL_SETTINGS_H
