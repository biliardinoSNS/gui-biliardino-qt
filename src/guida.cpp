#include "guida.h"
#include "ui_guida.h"

Guida::Guida(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Guida)
{
    ui->setupUi(this);
    ui->webView->setUrl(QUrl(QString("https://uz.sns.it/biliardino/guide/raspberry")));
}

Guida::~Guida()
{
    delete ui;
}
