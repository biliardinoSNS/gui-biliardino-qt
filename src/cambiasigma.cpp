#include "cambiasigma.h"
#include "ui_cambiasigma.h"

CambiaSigma::CambiaSigma(QWidget *parent, bool supergoal) :
    QDialog(parent),
    ui(new Ui::CambiaSigma)
{
    ui->setupUi(this);
    ui->btnOk->setEnabled(false);
    dialogLogin = nullptr;
    connect(this, SIGNAL(sigCambia()), parentWidget(), SLOT(cambiaSigma()));
    ui->doubleSpinBoxBlu->setValue(Arduino::sigma[Arduino::Porte::Blu]);
    ui->doubleSpinBoxRed->setValue(Arduino::sigma[Arduino::Porte::Red]);
    ui->doubleSpinBoxSGR->setValue(Arduino::sigma[Arduino::Porte::SRed]);
    ui->doubleSpinBoxSGB->setValue(Arduino::sigma[Arduino::Porte::SBlu]);
    this->supergoal = supergoal;
}

CambiaSigma::~CambiaSigma(){
    delete ui;
    if (dialogLogin != nullptr) delete dialogLogin;
}

void CambiaSigma::prendiGiocatore(Giocatore& g){
    ui->btnOk->setEnabled(true);
    giocatore = g;
    ui->doubleSpinBoxBlu->setReadOnly(false);
    ui->doubleSpinBoxRed->setReadOnly(false);
    if (supergoal){
        ui->doubleSpinBoxSGB->setReadOnly(false);
        ui->doubleSpinBoxSGR->setReadOnly(false);
    }
}

void CambiaSigma::on_btnLogIn_clicked(){
    dialogLogin = new LoginSingolo(this);
    dialogLogin->show();
}

void CambiaSigma::on_btnAnnulla_clicked(){
    close();
}

void CambiaSigma::on_btnOk_clicked(){
    Arduino::editSigma(ui->doubleSpinBoxRed->value(),
              ui->doubleSpinBoxBlu->value(),
              ui->doubleSpinBoxSGR->value(),
              ui->doubleSpinBoxSGB->value());
    emit sigCambia();
    close();
}
