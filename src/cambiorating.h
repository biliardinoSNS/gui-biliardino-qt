#ifndef CAMBIORATING_H
#define CAMBIORATING_H

#include <QDialog>
#include <QLabel>

#include "partita.h"

namespace Ui {
class CambioRating;
}

class CambioRating : public QDialog
{
    Q_OBJECT

public:
    explicit CambioRating(Partita p, QObject* appo, QWidget *parent = 0);
    ~CambioRating();

    QLabel* get_label(const Partita::Posizione& p);
    QLabel* get_foto(const Partita::Posizione& p);

 signals:
    void mandaGiocatore(const Giocatore, Partita::Posizione);
    
private slots:
  
private:
    Partita partita;
    Ui::CambioRating *ui;
};

#endif // CAMBIORATING_H
