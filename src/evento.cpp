#include "evento.h"

Evento::~Evento(){

}



curlpp::Forms Inizio::form() const {
    auto g = partita->get_giocatori();
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("collegio", col_breve(partita->collegio)));
    formParts.push_back(new curlpp::FormParts::Content("tipo", "I"));
    for (auto it = g.begin(); it != g.end(); it++){
        formParts.push_back(new curlpp::FormParts::Content(it->first.breve(), it->second.get_barcode()));
    }
    return formParts;
}

curlpp::Forms Fine::form() const {
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("idPartita", std::to_string(pkPart)));
    formParts.push_back(new curlpp::FormParts::Content("tipo", "F"));
    return formParts;
}


curlpp::Forms Swap::form() const {
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("idPartita", std::to_string(pkPart)));
    formParts.push_back(new curlpp::FormParts::Content("squadra", s_breve(squadra)));
    formParts.push_back(new curlpp::FormParts::Content("tipo", "S"));
    return formParts;
}


curlpp::Forms Annulla::form() const {
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("idPartita", std::to_string(pkPart)));
    formParts.push_back(new curlpp::FormParts::Content("tipo", "A"));
    formParts.push_back(new curlpp::FormParts::Content("squadra", s_breve(squadra)));
    return formParts;
}


curlpp::Forms Goal::form() const {
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("idPartita", std::to_string(pkPart)));
    formParts.push_back(new curlpp::FormParts::Content("squadra", s_breve(squadra)));
    formParts.push_back(new curlpp::FormParts::Content("tipo", tipo));
    return formParts;
}


void EventoPartita::gestisciRisposta(std::stringstream& s){
    std::string appo = s.str();
    if (appo == "success")
        return;
    throw badAnswer();
}


void Fine::gestisciRisposta(std::stringstream &s){
    std::string appo = s.str();
    Json::Value root;
    try {
        jsonFromStr(root, appo);
        for (auto it = old_partita.get_giocatori().begin(); it != old_partita.get_giocatori().end(); it++){
            it->second.copy_old_rating();
            int mediaA, sdevA, mediaD, sdevD;
            mediaA = root.get("ma" + it->first.breve(), 1500).asInt();
            sdevA = root.get("sa" + it->first.breve(), 20).asInt();
            mediaD = root.get("md" + it->first.breve(), 1500).asInt();
            sdevD = root.get("sd" + it->first.breve(), 1500).asInt();
            Giocatore::Rating ra(mediaA, sdevA);
            Giocatore::Rating rd(mediaD, sdevD);
            it->second.set_rating(Giocatore::Ruolo::Attacco, ra);
            it->second.set_rating(Giocatore::Ruolo::Difesa, rd);
        }
    }
    catch (std::exception& e){
        std::cerr << "Bad json decode" << std::endl;
    }
    catch (...){
        std::cerr << "Error in decoding" << std::endl;
    }
}



void Inizio::gestisciRisposta(std::stringstream &s){
    std::string nome, cognome, check;
    std::stringstream appos;
    appos << s.str();
    appos >> check >> nome >> cognome;
    if (check == "PLoccupato"){
        throw playerOccupato(nome, cognome);
    }
    int appo;
    s >> appo;
    if (s.fail()){
        throw badAnswer();
    }
    partita->set_pk(appo);
}
