#include <QtTest>
#include <QCoreApplication>
#include "mainwindow.h"


using namespace Server;

class MainTest : public QObject {
    Q_OBJECT

public:
    MainTest();
    ~MainTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();
private:
  MainWindow* main;
};

MainTest::MainTest(){
  main = new MainWindow();
}

MainTest::~MainTest(){
  delete main;
}

void MainTest::initTestCase(){
}

void MainTest::cleanupTestCase(){

}

void MainTest::test_case1(){
  qDebug() << "Starting mainwindow";
  QPushButton* inizia = main->findChild<QPushButton*>("btnIniziaPartita");
  QPushButton* goalBlu = main->findChild<QPushButton*>("btnAddGoalBlu");
  QPushButton* fine = main->findChild<QPushButton*>("btnFinePartita");

  qDebug() << "Trying to get player via barcode";
  Giocatore g1 = richiediPlayerViaBarcode("1");
  if (g1 == Giocatore::nessuno){
    QFAIL("No player got back");
  }

  qDebug() << "Initialising game";
  Giocatore g2, g3, g4;
  g2 = richiediPlayerViaBarcode("2");
  g3 = richiediPlayerViaBarcode("3");
  g4 = richiediPlayerViaBarcode("4");

  main->mettiInGioco(g1, g2, Partita::Squadra::Blu);
  main->mettiInGioco(g3, g4, Partita::Squadra::Red);

  qDebug() << "Starting game";
  QTest::mouseClick(inizia, Qt::LeftButton);
  QTest::mouseClick(goalBlu, Qt::LeftButton, Qt::KeyboardModifiers(), QPoint(), 2000);
  QTest::mouseClick(goalBlu, Qt::LeftButton, Qt::KeyboardModifiers(), QPoint(), 2000);
  QTest::mouseClick(goalBlu, Qt::LeftButton, Qt::KeyboardModifiers(), QPoint(), 2000);
  QTest::mouseClick(fine, Qt::LeftButton, Qt::KeyboardModifiers(), QPoint(), 2000);
  qDebug() << "End of game";

  QTest::qWait(5000);
  qDebug() << "Success";
}

QTEST_MAIN(MainTest)
#include "test_main.moc"


