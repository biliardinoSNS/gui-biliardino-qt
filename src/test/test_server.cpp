/**
 * @file test_server.cpp
 * @brief Vari test per provare il funzionamento delle API con il server.
 * @warning Funziona solo se il server è acceso.
 * @warning Registra davvero le partite sul server.
 * @warning È necessario che sul server web di test siano presenti 4 giocatori, con barcode 1,2,3,4.
 */


#include <QtTest>
#include <QCoreApplication>
#include "serverrequests.h"


using namespace Server;
using namespace std;

class ServerTest : public QObject {
    Q_OBJECT

public:
  ServerTest() {}
  ~ServerTest() {}

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();
    void test_case2();
    void test_case3();
private:
    std::list<Cookie> cookieList;
};


void ServerTest::initTestCase(){
  LOCAL::load_settings();
}

void ServerTest::cleanupTestCase(){

}

void ServerTest::test_case1(){
  qDebug() << "Trying to login";
  login(cookieList);
  qDebug() << "Trying to get player via barcode";
  Giocatore gioc = richiediPlayerViaBarcode("1");
  if (gioc == Giocatore::nessuno){
    QFAIL("No player got back");
  }

  qDebug() << "Initialising game";
  Giocatore g2, g3, g4;
  g2 = richiediPlayerViaBarcode("2");
  g3 = richiediPlayerViaBarcode("3");
  g4 = richiediPlayerViaBarcode("4");
  Partita::collegio = coll_fromStdStr(LOCAL::COLLEGIO);
  Partita p(gioc, g2, g3, g4, 0, 0);

  qDebug() << "Sending start to server";
  mandaInizioServer(p);

  qDebug() << "Sending cancer goals";
  mandaGoalServer("GP", p.get_pk(), "R");
  mandaGoalServer("GP", p.get_pk(), "R");
  mandaGoalServer("GP", p.get_pk(), "B");
  mandaGoalServer("GP", p.get_pk(), "B");
  mandaGoalServer("GP", p.get_pk(), "R");
  mandaGoalServer("GP", p.get_pk(), "B");
  mandaGoalServer("GP", p.get_pk(), "B");
  mandaGoalServer("GP", p.get_pk(), "B");
  mandaGoalServer("GP", p.get_pk(), "B");
  mandaGoalServer("GP", p.get_pk(), "B");
  mandaGoalServer("GP", p.get_pk(), "B");
  
  qDebug() << "Swapping red squad";
  mandaSwapServer(p.get_pk(), "R");
  qDebug() << "Removing a blue goal";
  mandaAnnullaServer(p.get_pk(), "B");
  qDebug() << "Sending end";
  mandaFineServer(p.get_pk());
  return;
}



void ServerTest::test_case2(){
  qDebug() << "Trying to login";
  login(cookieList);
  qDebug() << "Trying to get player via barcode";
  Giocatore gioc = richiediPlayerViaBarcode("1");
  if (gioc == Giocatore::nessuno){
    QFAIL("No player got back");
  }

  qDebug() << "Initialising game";
  Giocatore g2, g3, g4;
  g2 = richiediPlayerViaBarcode("2");
  g3 = richiediPlayerViaBarcode("3");
  g4 = richiediPlayerViaBarcode("4");
  Partita::collegio = coll_fromStdStr(LOCAL::COLLEGIO);
  Partita p(gioc, g2, g3, g4, 0, 0);

  qDebug() << "Sending start to server";
  mandaInizioServer(p);

  qDebug() << "Sending cancer goals";
  mandaSwapServer(p.get_pk(), "R");
  mandaGoalServer("GP", p.get_pk(), "R");
  mandaSwapServer(p.get_pk(), "R");
  mandaAnnullaServer(p.get_pk(), "R");
  mandaGoalServer("GP", p.get_pk(), "R");
  mandaGoalServer("GP", p.get_pk(), "B");

  qDebug() << "Swapping red squad";
  mandaSwapServer(p.get_pk(), "R");
  qDebug() << "Removing a blue goal";
  mandaAnnullaServer(p.get_pk(), "B");
  qDebug() << "Sending end";
  mandaFineServer(p.get_pk());
  return;
}

QTEST_GUILESS_MAIN(ServerTest)
#include "test_server.moc"


void ServerTest::test_case3(){
  qDebug() << "Trying to login";
  login(cookieList);
  qDebug() << "Trying to get player via barcode";
  Giocatore gioc = richiediPlayerViaBarcode("1");
  if (gioc == Giocatore::nessuno){
    QFAIL("No player got back");
  }

  qDebug() << "Initialising game";
  Giocatore g2, g3, g4;
  g2 = richiediPlayerViaBarcode("2");
  g3 = richiediPlayerViaBarcode("3");
  g4 = richiediPlayerViaBarcode("4");
  Partita::collegio = coll_fromStdStr(LOCAL::COLLEGIO);
  Partita p(gioc, g2, g3, g4, 0, 0);

  qDebug() << "Sending start to server";
  mandaInizioServer(p);

  qDebug() << "Sending cancer goals";
  mandaGoalServer("GP", p.get_pk(), "R");
  mandaGoalServer("GP", p.get_pk(), "R");

  Partita nuova;
  richiediPartiteInCorso(coll_fromStdStr(LOCAL::COLLEGIO), nuova);
  QCOMPARE((int) nuova.get_punteggi()[Partita::Squadra::Red], 2);
  QCOMPARE(nuova.get_pk(), p.get_pk());

  qDebug() << "Sending end";
  mandaFineServer(p.get_pk());
  return;
}
