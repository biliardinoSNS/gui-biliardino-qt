
/**
 * @file cambiasigma.h
 * @brief Finestra grafica per cambiare i parametri di calibrazione di Arduino.
 * Dialog che permette all'utente di comunicare ad Arduino i nuovi parametri di calibrazione. Dato che non vogliamo vandalismo, è necessario loggarsi per fare questa operazione. C'è una finestra di dialogo (LoginSingolo) che si può aprire e che attiva i bottoni in caso di buon login. Altrimenti i bottoni sono disattivati.
 */


#ifndef CAMBIASIGMA_H
#define CAMBIASIGMA_H

#include <map>
#include <string>

#include <QDialog>

#include "serverrequests.h"
#include "arduino.h"
#include "giocatore.h"
#include "loginsingolo.h"


namespace Ui {
class CambiaSigma;
}

class CambiaSigma : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Costruttore.
     * @param supergoal Se Arduino ha collegato anche le porte per il supergoal o meno.
     */
    explicit CambiaSigma(QWidget *parent = 0, bool supergoal = false);
    ~CambiaSigma();


signals:
    void sigCambia(); /**< La richiesta del cambio viene lanciata dalla MainWindow. Questo segnale comunica la richiesta alla MainWindow che a sua volta la inoltra */

public slots:
  void prendiGiocatore(Giocatore& g); /**< Serve per prendersi il giocatore dalla finestra di dialogo LoginSingolo */ 

private slots:
  void on_btnLogIn_clicked();
  void on_btnAnnulla_clicked();
  void on_btnOk_clicked();

private:
    Ui::CambiaSigma *ui;
    Giocatore giocatore;
    LoginSingolo* dialogLogin;
    bool supergoal;
};

#endif // CAMBIASIGMA_H
