#include "finepartita.h"
#include "ui_finepartita.h"


const unsigned int ContoAllaRovescia::DEFAULT_TIMEOUT_S = 10;


FinePartita::FinePartita(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FinePartita)
{
    ui->setupUi(this);

    countdown = new ContoAllaRovescia(this);
    countdown->start();
    connect(countdown, SIGNAL(set_messaggio(uint)), this, SLOT(set_messaggio(uint)));
    connect(countdown, SIGNAL(ok()), this, SLOT(on_btnOkKick_clicked()));
    connect(this, SIGNAL(sigOk()), parentWidget(), SLOT(finisci()));
    connect(this, SIGNAL(sigOkKick()), parentWidget(), SLOT(finisciKick()));
    connect(this, SIGNAL(sigAnnulla()), parentWidget(), SLOT(annullaFineAutomatica()));
}

FinePartita::~FinePartita()
{
    countdown->stop();
    delete ui;
    delete countdown;
}

void FinePartita::on_btnAnnulla_clicked()
{
    countdown->stop();
    close();
}

void FinePartita::on_btnOk_clicked()
{
    countdown->stop();
    emit sigOk();
    close();
}

void FinePartita::on_btnOkKick_clicked()
{
    countdown->stop();
    emit sigOkKick();
    close();
}

void FinePartita::set_messaggio(uint i){
    std::string m("<html><head/><body><p>Siete arrivati al limite punti di default.</p><p>La partita verrà terminata automaticamente fra ");
    std::stringstream s;
    s << i;
    std::string appo;
    s >> appo;
    m += appo;
    m += " secondi.</p></body></html>";
    ui->MessaggioFine->setText(QString::fromStdString(m));
}

void FinePartita::close_se_goal(Partita::EventoSquadra e){
    if (e.evento == Partita::tipoEvento::GOAL_F || e.evento == Partita::tipoEvento::SUPERGOAL || e.evento == Partita::tipoEvento::GOAL_ANN || e.evento == Partita::tipoEvento::GOAL_B)
        on_btnAnnulla_clicked();
}
