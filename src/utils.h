/**
 * @file utils.h
 * @brief File con alcune funzioni utili da utilizzare nel progetto.
 */

#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <sstream>
#include <iostream>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/writer.h>

/**
 * @brief fromStr prende una stringa e restituisce un intero.
 * @param s la stringa da decodificare
 * @return Il numero 
 */
int fromStr(std::string s);


/**
 * @brief decode cifratura idempotente. Data una stringa ne restituisce un altra. Se viene chiamata di nuovo sulla seconda stringa, restiuisce la stringa di partenze.
 * È fatto con un metodo scemo, uno XOR, ma è meglio di niente.
 * @param input
 * @return
 */
std::string decode(const std::string& input);


/**
 * @brief Prende una variabile d'ambiente e se non c'è restituisce un default.
 * @param variable_name è il nome della variabile d'ambiente.
 * @param default_value esplicativo.
 * @return Restituisce la stringa corrispondente alla variabile d'ambiente.
 */
std::string get_env_or_default(const std::string& variable_name, const std::string& default_value);

void jsonFromStr(Json::Value& root, std::string s);

#endif // UTILS_H
