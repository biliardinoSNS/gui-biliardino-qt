#include "plot.h"
#include "ui_plot.h"

Plot::Plot(bool sg, Arduino::Porte porta, Arduino* ard, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Plot)
{
    ui->setupUi(this);
    supergoal = sg;
    arduino = ard;
    this->porta = porta;
    loraesatta = QTime::currentTime();
    lastPointKey = 0;
    connect(this, SIGNAL(sigStop()), parent, SLOT(stopPlot()));
    if (porta == Arduino::Porte::Tutte){
        ui->customPlot->addGraph(); // red line
        ui->customPlot->graph(0)->setPen(QPen(QColor(255, 110, 40)));
        ui->customPlot->addGraph(); // blue line
        ui->customPlot->graph(1)->setPen(QPen(QColor(40, 110, 255)));
        if (supergoal){
            ui->customPlot->addGraph(); // red line
            ui->customPlot->graph(2)->setPen(QPen(QColor(255, 30, 0)));
            ui->customPlot->addGraph(); // blue line
            ui->customPlot->graph(3)->setPen(QPen(QColor(0, 30, 255)));
        }
    }
    else {
        ui->customPlot->addGraph();
        ui->customPlot->graph(0)->setPen(QPen(QColor(255,255,255)));
    }
    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%h:%m:%s");
    ui->customPlot->xAxis->setTicker(timeTicker);
    ui->customPlot->axisRect()->setupFullAxesBox();
    ui->customPlot->yAxis->setRange(0,300);
    ui->customPlot->yAxis->setRange(0, 1024);


    // make left and bottom axes transfer their ranges to right and top axes:
    connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));

    // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
    connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
    dataTimer.start(0); // Interval 0 means to refresh as fast as possible

}

Plot::~Plot() {
    delete ui;
}

void Plot::closeEvent(QCloseEvent* event){
    emit sigStop();
    dataTimer.stop();
    event->accept();
}



void Plot::realtimeDataSlot(){
    double key = loraesatta.elapsed()/1000.0; // time elapsed since start of demo, in seconds
    int massimo = 10;
    unsigned int quanti = 0;
    if (key-lastPointKey > 0.005) { // at most add point every 100 ms
        std::string messaggio;
        if (arduino->canReadLine()){
            messaggio = arduino->readLine();
        }

        if (porta == Arduino::Porte::Tutte) {
            if (supergoal){
                int a[4];
                if (std::sscanf(messaggio.c_str(), "<r%db%dR%dB%d>", a, a+1, a+2, a+3) == 4){
                    ui->customPlot->graph(0)->addData(key, (double) a[0]);
                    ui->customPlot->graph(1)->addData(key, (double) a[1]);
                    ui->customPlot->graph(2)->addData(key, (double) a[2]);
                    ui->customPlot->graph(3)->addData(key, (double) a[3]);
                 }
                quanti = 4;
            }
            else {
                int a[2];
                if (std::sscanf(messaggio.c_str(), "<r%db%d>", a, a+1) == 2){
                    ui->customPlot->graph(0)->addData(key, (double) a[0]);
                    ui->customPlot->graph(1)->addData(key, (double) a[1]);
                }
                quanti = 2;
            }
        }
        else {
            int a;
            if (std::sscanf(messaggio.c_str(), "<%d>", &a) == 1){
                ui->customPlot->graph(0)->addData(key, (double) a);
            }
            quanti = 1;
        }
        lastPointKey = key;
    }
    // make key axis range scroll with the data (at a constant range size of 8):
    for (unsigned int j = 0; j < quanti; j++){
        bool success;
        QCPRange range = ui->customPlot->graph(j)->getValueRange(success);
        if (success && massimo < range.upper){
            massimo = range.upper;
        }
    }
    ui->customPlot->xAxis->setRange(key, 8, Qt::AlignRight);
    ui->customPlot->replot();
}
