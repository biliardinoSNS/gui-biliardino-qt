/**
 * @file evento.h 
 * @brief La classe Evento e tutte le sue derivate servono per la gestione delle richieste al server e duelle sue risposte con dei thread
 */



#ifndef EVENTO_H
#define EVENTO_H

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Info.hpp>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/writer.h>
#include <string>
#include <sstream>
#include <exception>

#include "utils.h"
#include "partita.h"
#include "local_settings.h"


/**
 * @brief Classe completamente astratta per la gestione degli eventi
 * La comunicazione con il server avviene tramite API definite in serverrequests.h. 
 * In particolare, per segnare un goal, iniziare, finire, annullare un goal, swappare dei giocatori, 
 * serve fare una richiesta HTTP POST. Per questo motivo c'è il metodo pure virtual forms() che restituisce proprio il tipo di form voluto.
 */
class Evento {
public:
  enum class tipiEvento { inizio, fine, goal, annulla, swap }; /**< I tipi di eventi che possono essere gestiti */
  virtual ~Evento() = 0; /**< Distruttore pure virtual */
  virtual curlpp::Forms form() const = 0; /**< Restituisce il form compilato che dipende dal tipo di evento. Serve per le richieste HTTP POST al server */
  virtual std::string url() const { return LOCAL::BASE_URL_REQUEST + "set/"; } /**< Le richieste non si fanno tutte nello stesso posto, questo metodo serve per l'appunto ad indicare dove farle */
  virtual void gestisciRisposta(std::stringstream& s) = 0; /**< Metodo per la gestione della risposta, utilizzata in attendirisposta.h in thread separati */
  virtual tipiEvento tipoEvento() const = 0; /**< Serve per identificare il tipo e per farci uno static_cast */
};


/**
 * @brief Un evento a cui è associata una partita di cui si conosce già il pk e va comunicato al server. Un esempio è un goal fatto, un esempio di ciò che non è è un IniziaPartita
 *
 */
class EventoPartita {
public:
    EventoPartita(unsigned int i) { pkPart = i; }
    EventoPartita(const EventoPartita& rhs) { pkPart = rhs.pkPart; }
    virtual void gestisciRisposta(std::stringstream& s);
    virtual ~EventoPartita() {}
protected:
    unsigned int pkPart;
};


/**
 * @brief Un evento a cui è associata sia una partita che una squadra. Per esempio un goal fatto o un goal annullato, ma anche uno swap.
 */
class EventoSquadra : public Evento, public EventoPartita {
public:
    EventoSquadra(Partita::Squadra s, unsigned int i) : EventoPartita(i) { squadra = s; }
    EventoSquadra(const EventoSquadra& rhs) : EventoPartita(rhs) { squadra = rhs.squadra; }
    virtual ~EventoSquadra() {}
    virtual void gestisciRisposta(std::stringstream& s)  { return EventoPartita::gestisciRisposta(s); }
    std::string get_squadra_str() const { return s_breve(squadra); }
    Partita::Squadra get_squadra() const { return squadra; }
protected:
    Partita::Squadra squadra;
};


/**
 * @brief Classe derivata per la gestione dell'inizio di una partita.
 * Il puntatore a partita serve per poter modificare il pk della partita che viene per l'appunto mandato dal server nella sua risposta.
 * Per comunicare l'inizio, nel form ci deve essere il collegio in cui si trova la partita, informazione riportata dentro *partita.
 */
class Inizio : public Evento {
public:
    Inizio(Partita* part) { partita = part; }
    Inizio(const Inizio& rhs) { partita = rhs.partita; }
    void gestisciRisposta(std::stringstream &s);
    tipiEvento tipoEvento() const { return tipiEvento::inizio; }
    curlpp::Forms form() const;
    ~Inizio() {}


    /**
     * @brief Classe di cui fare il throw quando un giocatore sta giocando in un altro collegio. La GUI se ne accorge dalla risposta del server.
     */
    class playerOccupato : public std::exception {
    public: virtual const char* what() throw() { return ("Il seguente giocatore è registrato in un altro collegio: " + g.get_nome()).c_str(); }
        playerOccupato(Giocatore& gg) { g = gg; }
        playerOccupato(std::string nome, std::string cognome) { Giocatore gg(nome + cognome, "", -1); g = gg;}
    private:
        Giocatore g;
    };

private:
    Partita* partita;
};


/**
 * @brief Gestione della fine della partita.
 * Il form contiene il pk della partita che si conosce grazie al costruttore.
 * La gestione della risposta è ereditata direttamente da EventoPartita
 */
class Fine : public Evento, public EventoPartita {
public:
    Fine(unsigned int pk, const Partita p) : EventoPartita(pk) { old_partita = p; }
    Fine(const Fine& rhs) : EventoPartita(rhs) {}
    ~Fine() { return; }
    curlpp::Forms form() const;
    void gestisciRisposta(std::stringstream& s);
    tipiEvento tipoEvento() const { return tipiEvento::fine; }

    Partita get_partita() const { return old_partita; }

private:
    Partita old_partita;
};


/**
 * @brief Gestione dei goal segnati.
 * Bisogna inviare il pk della partita, la squadra e il tipo di goal fatto (può essere fotocellula, supegoal, goal dal pc, goal dal bottone)
 */
class Goal : public EventoSquadra {
public:
    Goal(std::string t, Partita::Squadra s, unsigned int pk) : EventoSquadra(s, pk) { tipo = t; }
    Goal(const Goal& rhs) : EventoSquadra(rhs) { tipo = rhs.tipo; }
    curlpp::Forms form() const;
    void gestisciRisposta(std::stringstream& s) { return EventoPartita::gestisciRisposta(s); }
    tipiEvento tipoEvento() const { return tipiEvento::goal; }
private:
    std::string tipo;
};



/**
 * @brief Gestione degli swap.
 * Basta inviare la squadra e il pk della partita.
 */

class Swap : public EventoSquadra {
public:
    Swap(Partita::Squadra s, unsigned int pk) : EventoSquadra(s, pk) {}
    Swap(const Swap& rhs) : EventoSquadra(rhs) {}
    curlpp::Forms form() const;
    void gestisciRisposta(std::stringstream& s) { return EventoPartita::gestisciRisposta(s); }
    tipiEvento tipoEvento() const { return tipiEvento::swap; }
};


/**
 * @brief Annulla un goal segnato.
 * Si invia il pk e la squadra, non serve altro.
 */
class Annulla : public EventoSquadra {
public:
    Annulla(Partita::Squadra s, unsigned int pk): EventoSquadra(s, pk) {}
    Annulla(const Annulla& rhs) : EventoSquadra(rhs) {}
    curlpp::Forms form() const;
    void gestisciRisposta(std::stringstream& s) { return EventoPartita::gestisciRisposta(s); }
    tipiEvento tipoEvento() const { return tipiEvento::annulla; }
};


/**
 * Eccezione generica che viene lanciata quando il server risponde in modo inaspettato.
 */
class badAnswer : public std::exception {
public: virtual const char* what() const throw()  { return "Il server ha lanciato un eccezione e non ha risposto come voluto."; }
};



#endif // EVENTO_H
