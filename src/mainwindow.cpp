#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :    QMainWindow(parent),   ui(new Ui::MainWindow) {
    ui->setupUi(this);


    LOCAL::load_settings();
    Partita::collegio = coll_fromStdStr(LOCAL::COLLEGIO);
    this->setWindowTitle(QString::fromStdString("GUI Biliardino " + col_lungo(Partita::collegio)));
    Giocatore::set_foto_dir();
    supergoal = LOCAL::supergoal;
    if (!supergoal){
        menuSupergoal();
    }

    /** richiede se ci sono partite in corso e in caso contrario ne inizializza una vuota */
    try {
        Server::login(Server::cookieList);
        inCorso = Server::richiediPartiteInCorso(Partita::collegio, partita);
	changeButtonPalette(inCorso);
    }
    catch (curlpp::RuntimeError& e){
        std::cerr << e.what() << std::endl;
        inCorso = false;
    }
    catch (curlpp::LogicError& e){
        std::cerr << e.what() << std::endl;
        inCorso = false;
    }
    catch (Server::permissionDenied& e){
        printLogger(std::string("Il server non ha risposto come voluto! Potrebbe essere disponibile una partita non recuperata"), e);
        inCorso = false;
        Server::login(Server::cookieList);
    }

    checkArdu = nullptr;
    aggiornaNomiFoto();
    aggiornaPunteggi(false);
    Arduino::editSigma(3., 3., 3., 3.);
    try {
        collegaArduino();
        aggiornaPunteggi();
    }
    catch (Arduino::noArd& e){
        printLogger(std::string("Arduino non trovato."), e);
    }
    checkArdu = new controllaSigArduino(this, &arduino);
    checkArdu->start();
    connect(this, SIGNAL(nuovoArd(Arduino*)), checkArdu, SLOT(prendiArd(Arduino*)));
    connect(this, SIGNAL(fermaThread()), checkArdu, SLOT(ferma()));
}



MainWindow::~MainWindow()
{
    delete ui;
    checkArdu->stop();
    checkArdu->wait(1000);

}


void MainWindow::changeButtonPalette(bool b){
    QPalette pal = ui->centralWidget->palette();
    if (b) {
        pal.setBrush(QPalette::Button, Qt::darkGreen);
    }
    else {
        QRgb col = qRgb(239,235,231);
        pal.setBrush(QPalette::Button, QColor(col));
    }
    ui->centralWidget->setPalette(pal);
}

void MainWindow::iniziaPartita()
{
    try {
        Inizio* ev = new Inizio(&partita);
        attendiRisposta(ev);
        set_inCorso(true);
        partita.set_punteggi(0,0);
        changeButtonPalette(true);
        riproduciAudio(tipoAudio::INIZIO);
        arduino.ardInizio();
	printLogger(Messaggio::CommonMessage::InitSuccess);
        return;
    }
    catch (Server::erroreServer& e){
        printLogger(Messaggio::CommonMessage::NoAnswer, e);
        return;
    }
    catch (Server::permissionDenied& e){
        Server::login(Server::cookieList);
        printLogger(Messaggio::CommonMessage::PermDenied, e);
    }

    catch (Arduino::noArd& e){
        printLogger(Messaggio::CommonMessage::InitSuccess);
        printLogger(Messaggio::CommonMessage::NoArd, e);
    }
}


void MainWindow::on_btnIniziaPartita_clicked()
{
    if (get_inCorso()){
        printLogger(std::string("Non puoi cominciare una partita se ce n'é già una in corso!"), Messaggio::Tipo::warn);
        return;
    }
    if (partita.controllaNessuno()){
        printLogger(std::string("Tutti i giocatori devono essere loggati per iniziare una nuova partita!"), Messaggio::Tipo::warn);
        return;
    }
    if (partita.controllaPlayerUguali()){
       printLogger(std::string("Non puoi giocare contro te stesso! C'è lo stesso giocatore in due punti diversi della partita!"),  Messaggio::Tipo::warn);
       return;
    }
    iniziaPartita();
}


void MainWindow::setPixmapFromPercorso(std::string percorso, QLabel* bersaglio){
    QString url = QString::fromStdString(percorso); // nell'esempio trovato su internet era R"/cusu/mano/macchiato.jpg"
    QPixmap img(url);
    bersaglio->setPixmap(img);
}


QLabel* MainWindow::get_foto(const std::string flag) const{
    if (flag == "AB") return this->ui->fotoAB;
    if (flag == "AR") return this->ui->fotoAR;
    if (flag == "DB") return this->ui->fotoDB;
    if (flag == "DR") return this->ui->fotoDR;
    throw "FLAG PLAYER SBAGLIATO";
    return nullptr;
}

QLabel* MainWindow::get_foto(const Partita::Posizione& p) const{
    return get_foto(p.breve());
}


QLabel* MainWindow::get_label(const std::string flag) const{
    if (flag == "AB") return this->ui->labelNomeAB;
    if (flag == "AR") return this->ui->labelNomeAR;
    if (flag == "DB") return this->ui->labelNomeDB;
    if (flag == "DR") return this->ui->labelNomeDR;
    throw "FLAG PLAYER SBAGLIATO";
    return nullptr;
}

QLabel* MainWindow::get_label(const Partita::Posizione &p) const{
    return get_label(p.breve());
}

QLCDNumber* MainWindow::get_LCD(const Partita::Squadra& s) const{
    switch (s) {
    case Partita::Squadra::Blu: return  this->ui->displayPunteggioB; break;
    case Partita::Squadra::Red: return this->ui->displayPunteggioR; break;
    default: throw "SQUADRA CANCRO"; break;
    }
}

QLCDNumber* MainWindow::get_LCD(const std::string flag) const{
    if (flag == "R") return this->ui->displayPunteggioR;
    if (flag == "B") return this->ui->displayPunteggioB;
    throw "FLAG COLORE SBAGLIATO";
    return nullptr;
}


void MainWindow::aggiornaNomiFoto(){
    for (auto it = partita.get_giocatori().begin(); it != partita.get_giocatori().end(); ++it){
        QLabel* lab = get_label(it->first);
        QLabel* foto = get_foto(it->first);
        if (it->first.get_ruolo() == Giocatore::Ruolo::Attacco) {
            lab->setText(QString::fromStdString(it->second.get_nome() + ", " + it->second.get_rating_atk()));
        }
        else
            lab->setText(QString::fromStdString(it->second.get_nome() + ", " + it->second.get_rating_def()));
        foto->setPixmap(QPixmap(QString::fromStdString(Giocatore::PERCORSO_FOTO + it->second.get_foto())).scaledToWidth(200));

    }
}

void MainWindow::aggiornaPunteggi(bool avvertiArd){
    auto punteggi_local = partita.get_punteggi();
    for (auto it = punteggi_local.begin(); it != punteggi_local.end(); ++it){
        get_LCD(it->first)->display((int) it->second);
    }
    if (avvertiArd){
        try{
            arduino.scriviPunteggio(punteggi_local[Partita::Squadra::Red], punteggi_local[Partita::Squadra::Blu]);
        }
        catch (Arduino::noArd& e){
            printLogger(Messaggio::CommonMessage::NoArd, e);
        }
    }
}

void MainWindow::on_btnAddCoda_clicked()
{
    InserisciNuovaCoda* dialogInserisciNuovaCoda = new InserisciNuovaCoda(this);
    dialogInserisciNuovaCoda->show();
}



void MainWindow::printLogger(Messaggio m){
    m.print();
    ui->textLogger->append(m.msgLog());
}



QString MainWindow::Messaggio::msgLog() const {
    std::string colore;
    switch (tipo) {
    case Tipo::info: colore = "Green"; break;
    case Tipo::warn: colore = "#bfb439"; break;
    case Tipo::error: colore = "Red"; break;
    }
    QString ret = QString::fromStdString("<font color=\"" + colore + "\">" + messaggioGUI + "</font>");
    return ret;
}




void MainWindow::addGoal(Partita::Squadra s, std::string tipo){
    if (!get_inCorso()){
        printLogger(std::string("Non ci sono partite in corso, non puoi aggiungere un goal!"), Messaggio::Tipo::warn); return;
    }
    try {
        Goal* ev = new Goal(tipo, s, partita.get_pk());
        attendiRisposta(ev);
        punti(s, 1);
        printLogger(Messaggio::CommonMessage::GoalSuccess, s);
        if ( partita.fine() ){
            if (true){// qui bisogna anche vedere se c'è coda
                FinePartita* dialogFinePartita = new FinePartita(this);
                connect(checkArdu, SIGNAL(sigEvento(Partita::EventoSquadra)), dialogFinePartita, SLOT(close_se_goal(Partita::EventoSquadra)));
                dialogFinePartita->show();
                riproduciAudio(tipoAudio::ALERTFINE);
            }
        }
    }
    catch (Server::erroreServer& e) {
        printLogger(Messaggio::CommonMessage::NoAnswer, e);
        return;
    }
    catch (Server::permissionDenied& e){
        Server::login(Server::cookieList);
        printLogger(Messaggio::CommonMessage::PermDenied, e);
    }
}


void MainWindow::punti(Partita::Squadra s, int pt){
    partita.edit_punteggi(s, pt, !coda.empty());
    aggiornaPunteggi();
}


void MainWindow::togliGoal(Partita::Squadra s){
    if (!get_inCorso()){
        printLogger(std::string("Nessuna partita in corso, non puoi togliere goal!"), Messaggio::Tipo::warn);
        return;
    }
    if (partita.get_punteggi()[s] <= 0){
        printLogger(std::string("La squadra è attualmente a zero, non puoi togliere altri goal!"), Messaggio::Tipo::warn);
        return;
    }
    try {
        Annulla* ev = new Annulla(s, partita.get_pk());
        attendiRisposta(ev);
        punti(s, -1);
        printLogger(Messaggio::CommonMessage::AnnSuccess, s);
    }
    catch (Server::erroreServer& e) {
        printLogger(Messaggio::CommonMessage::NoAnswer, e);
    }
    catch (Server::permissionDenied& e){
        Server::login(Server::cookieList);
        printLogger(Messaggio::CommonMessage::PermDenied, e);
    }
}


void MainWindow::finisciPartita(){
    if (!get_inCorso()){
        printLogger(std::string("Non puoi finire la partita se non ce n'è nessuna in corso!"), Messaggio::Tipo::warn);
        return;
    }
    try {
        partitaDaRecuperare = partita;
        Partita partitaVecchia = partita;
        Fine* ev = new Fine(partita.get_pk(), partitaVecchia);
        attendiRisposta(ev);
    }
    catch (Server::erroreServer& e){
        printLogger(Messaggio::CommonMessage::NoAnswer, e);
        return;
    }
    catch (Server::permissionDenied& e){
        Server::login(Server::cookieList);
        printLogger(Messaggio::CommonMessage::PermDenied, e);
        return;
    }

    partita.set_pk(-1);
    try{
        arduino.ardFine(partita.vincitore());
    }
    catch (Arduino::noArd& e){
        printLogger(Messaggio::CommonMessage::NoArd, e);
    }

    partita.set_punteggi(0,0);
    aggiornaPunteggi();
    set_inCorso(false);
    printLogger(Messaggio::CommonMessage::FineSuccess);
    changeButtonPalette(false);
    riproduciAudio(tipoAudio::FINE);
    return;
}


void MainWindow::eseguiSwap(Partita::Squadra s){
    Partita::Posizione a(Giocatore::Ruolo::Attacco, s), d(Giocatore::Ruolo::Difesa, s);
    if (partita.get_giocatori()[a] == Giocatore::nessuno || partita.get_giocatori()[d] == Giocatore::nessuno){
        printLogger(std::string("Non ci sono giocatori registrati!"), Messaggio::Tipo::warn); return;
    }
    Giocatore appo = partita.get_giocatori()[a];
    partita.get_giocatori()[a] = partita.get_giocatori()[d];
    partita.get_giocatori()[d] = appo;
    aggiornaNomiFoto();
    printLogger(Messaggio::CommonMessage::SwapSuccess, s);
    if (get_inCorso()){
        Swap* ev = new Swap(s, partita.get_pk());
        attendiRisposta(ev);
    }
    riproduciAudio(tipoAudio::SWAP);
}


void MainWindow::kick(Partita::Squadra s){
    if (get_inCorso()){
        printLogger(std::string("Caro figlio della merda, non si kicka qualcuno mentre gioca! Finisci prima la partita in corso."), Messaggio::Tipo::warn); return;
    }
    if (partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, s)] == Giocatore::nessuno){
        printLogger(std::string("Non c'è nessuno, chi pensi di kickare?"), Messaggio::Tipo::warn);
        return;
    }
    partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, s)] = Giocatore::nessuno;
    partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Difesa, s)] = Giocatore::nessuno;
    aggiornaNomiFoto();
}

void MainWindow::kickECoda(Partita::Squadra s) {
    if (get_inCorso()){
        printLogger(std::string("Caro figlio della merda, non si kicka qualcuno mentre gioca! Finisci prima la partita in corso."), Messaggio::Tipo::warn); return;
    }
    if (partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, s)] == Giocatore::nessuno){
        printLogger(std::string("Non c'è nessuno, chi pensi di kickare?"), Messaggio::Tipo::warn); return;
    }
    addCoda(partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, s)], partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Difesa, s)]);
    partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, s)] = Giocatore::nessuno;
    partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Difesa, s)] = Giocatore::nessuno;
    aggiornaNomiFoto();
}


void MainWindow::scalaCoda(Partita::Squadra s){
    if (get_coda().size() == 0){
        printLogger(std::string("Non ci sono giocatori in fila, aggiungili!"), Messaggio::Tipo::warn); return;
    }
    if (get_inCorso()){
        printLogger(std::string("C'è una partita in corso, prima termina e poi scala!"), Messaggio::Tipo::warn);
    }
    // Rimette in coda i giocatori in campo quando scala
    if (!(partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, s)] == Giocatore::nessuno)){
        kickECoda(s);
    }
    mettiInGioco(coda.front().atk, coda.front().def, s);
    coda.pop_front();
    ui->listaCoda->takeItem(0);
    aggiornaNomiFoto();
}


void MainWindow::mettiInGioco(Giocatore &atk, Giocatore &def, Partita::Squadra s){
  partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, s)] = atk;
  partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Difesa, s)] = def;
}


void MainWindow::finisciPartitaKick(){
    Partita::Squadra s;
    if (partita.get_punteggi()[Partita::Squadra::Red] > partita.get_punteggi()[Partita::Squadra::Blu])
        s = Partita::Squadra::Blu;
    else if (partita.get_punteggi()[Partita::Squadra::Red] < partita.get_punteggi()[Partita::Squadra::Blu])
        s = Partita::Squadra::Red;
    else {
        printLogger(std::string("Stai pareggiando, come faccio a decidere chi kickare?"), Messaggio::Tipo::warn);
        return;
    }
    finisciPartita();
    kickECoda(s);
    scalaCoda(s);
}

void MainWindow::annullaFineAutomatica() {
    partita.annulla_vantaggi();
}

void MainWindow::togliCoda(){
    if (get_coda().size() == 0){
        printLogger(std::string("Smettila di togliere giocatori quando non ci sono!"), Messaggio::Tipo::warn); return;
    }

    int indice = ui->listaCoda->currentRow();
    if (indice < 0){
        printLogger(std::string("Non puoi togliere la coppia selezionata dalla coda se non ne hai selezionata nessuna!"), Messaggio::Tipo::warn); return;
    }
    ui->listaCoda->takeItem(indice);
    eliminaCoda(indice);
}

void MainWindow::addCoda(Giocatore& atk, Giocatore& def){
    Coppia nuovi(atk, def);
    coda.push_back(nuovi);
    QListWidgetItem* n = new QListWidgetItem();
    n->setText(QString::fromStdString(nuovi.nome()));
    n->setTextAlignment(Qt::AlignCenter);
    ui->listaCoda->addItem(n);
}



void MainWindow::plot(Arduino::Porte p){
    emit fermaThread();
    try {
        checkArdu->stop();
        arduino.chiediPlot(p);
        Plot* grafico = new Plot(supergoal, p, &arduino, this);
        grafico->show();
    }
    catch (Arduino::noArd& e){
        printLogger(Messaggio::CommonMessage::NoArd, e);
    }
}

void MainWindow::collegaArduino(){
    arduino.reset();
    if (!arduino.esiste()) throw Arduino::noArd();
}


void MainWindow::ricollegaArd(){
    try{
        collegaArduino();
    }
    catch (Arduino::noArd& e){
        std::cerr << "Tentativo automatico di ricollegare arduino fallito: " << e.what() << std::endl;
    }
}


void MainWindow::on_toolbarRicollegaArduino_triggered(){
    ricollegaArd();
}




void MainWindow::on_toolbarCalibra_triggered(){
    try {
        arduino.calibra();
    }
    catch (Arduino::noArd& e){
        printLogger(Messaggio::CommonMessage::NoArd, e);
    }
}



void MainWindow::cambiaSigma(){
    try {
        arduino.cambiaSigma(supergoal);
    }
    catch (Arduino::noArd& e){
        printLogger(Messaggio::CommonMessage::NoArd, e);
    }

}



void MainWindow::on_toolbarCambiaSigma_triggered(){
    CambiaSigma* dialogCambiaSigma = new CambiaSigma(this, supergoal);
    dialogCambiaSigma->show();
}



void MainWindow::closeEvent(QCloseEvent *event){
    checkArdu->stop();
    event->accept();
}


void MainWindow::on_actionGuida_triggered(){
    Guida* dialogGuida = new Guida(this);
    dialogGuida->show();
}

void MainWindow::menuSupergoal(){
    ui->actionSupergoal_rossa->setDisabled(true);
    ui->actionSupergoal_blu->setDisabled(true);
    return;
}

void MainWindow::eventoArduino(Partita::EventoSquadra e){
    switch (e.evento) {
    case Partita::tipoEvento::GOAL_ANN:
        togliGoal(e.squadra); break;
    case Partita::tipoEvento::GOAL_F:
        addGoal(e.squadra, "GF"); break;
    case Partita::tipoEvento::GOAL_B:
        addGoal(e.squadra, "GB"); break;
    case Partita::tipoEvento::SUPERGOAL:
        addGoal(e.squadra, "SG"); break;
    default:
        break;
    }
}

void MainWindow::attendiRisposta(Evento *ev){
    AttendiRisposta* nuovoTh = new AttendiRisposta(this, ev);
    eventi.push(nuovoTh);
    nuovoTh->start();
}

void MainWindow::gestisciErroreServer(Evento *ev){
    switch (ev->tipoEvento()) {
    case Evento::tipiEvento::annulla:
        punti(static_cast<Annulla*>(ev)->get_squadra(), 1);
        break;
    case Evento::tipiEvento::fine:
        partita = partitaDaRecuperare;
        inCorso = true;
        break;
    case Evento::tipiEvento::goal:
        punti(static_cast<Goal*>(ev)->get_squadra(), -1);
        break;
    case Evento::tipiEvento::inizio:
        set_inCorso(false);
        partita.set_punteggi(0,0);
        break;
    case Evento::tipiEvento::swap: {
        Partita::Squadra s = static_cast<Swap*>(ev)->get_squadra();
        Giocatore appo = partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, s)];
        partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, s)] = partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Difesa, s)];
        partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Difesa, s)] = appo;
        break;
    }
    default:
        std::cerr << "Non è possibile che tu sia qui." << std::endl;
        break;
    }
    aggiornaNomiFoto();
    aggiornaPunteggi();
    printLogger(Messaggio::CommonMessage::AnnullamentoAzione);
    riproduciAudio(tipoAudio::ERRORE);
}

void MainWindow::stopPlot(){
    arduino.stopPlot();
    checkArdu = new controllaSigArduino(this, &arduino);
    checkArdu->start();
    connect(this, SIGNAL(nuovoArd(Arduino*)), checkArdu, SLOT(prendiArd(Arduino*)));
    connect(this, SIGNAL(fermaThread()), checkArdu, SLOT(ferma()));
}

void MainWindow::on_btnKickECodaRed_clicked() {
    kickECoda(Partita::Squadra::Red);
}

void MainWindow::on_btnKickECodaBlu_clicked() {
    kickECoda(Partita::Squadra::Blu);
}

void MainWindow::on_btnKickRed_clicked() {
    kick(Partita::Squadra::Red);
}

void MainWindow::on_btnKickBlu_clicked() {
    kick(Partita::Squadra::Blu);
}

void MainWindow::on_btnCambioCampo_clicked() {
    if (get_inCorso()){
        printLogger(std::string("Caro figlio della merda, non si cambia campo durante una partita! Finisci prima quella in corso."), Messaggio::Tipo::warn); return;
    }
    std::swap(partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, Partita::Squadra::Red)], partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Attacco, Partita::Squadra::Blu)]);
    std::swap(partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Difesa, Partita::Squadra::Red)], partita.get_giocatori()[Partita::Posizione(Giocatore::Ruolo::Difesa, Partita::Squadra::Blu)]);
    aggiornaNomiFoto();
}

void MainWindow::muoviCoda(int dir) {
    QListWidgetItem* current = ui->listaCoda->currentItem();
    int currIndex = ui->listaCoda->row(current);
    if (currIndex < 0) {
        printLogger(std::string("Non puoi muovere una coppia della coda se non ne hai selezionata nessuna!"), Messaggio::Tipo::warn); return;
    }
    if ((dir < 0 && currIndex == 0) || (dir > 0 && currIndex == (int)coda.size() - 1)) {
        //printLogger(Messaggio("Non puoi muovere una coppia in questa direzione: uscirebbe dalla coda!", Messaggio::Tipo::warn));
        return;
    }

    QListWidgetItem* other = ui->listaCoda->item(ui->listaCoda->row(current) + dir);
    int otherIndex = ui->listaCoda->row(other);

    QListWidgetItem *temp = ui->listaCoda->takeItem(otherIndex);
    ui->listaCoda->insertItem(currIndex, temp);
    ui->listaCoda->insertItem(otherIndex, current);

    auto it = coda.begin();
    if (dir < 0)
        currIndex = otherIndex;
    std::advance(it, currIndex);
    std::swap(*it, *std::next(it));
}

void MainWindow::on_btnCodaSu_clicked() {
    muoviCoda(-1);
}

void MainWindow::on_btnCodaGiu_clicked() {
    muoviCoda(1);
}

void MainWindow::riproduciAudio(tipoAudio t){
    QMediaPlayer* player = new QMediaPlayer();
    std::string path = get_audio_url(t);
    player->setMedia(QUrl::fromLocalFile(QString::fromStdString(path)));
    player->setVolume(80);
    player->play();
}

std::string MainWindow::get_audio_url(tipoAudio t){
    switch (t){
        case tipoAudio::FINE:
            return (LOCAL::PERCORSO_AUDIO + "fine.mp3"); break;
        case tipoAudio::ALERTFINE:
            return (LOCAL::PERCORSO_AUDIO + "alert_fine.mp3"); break;
        case tipoAudio::INIZIO:
            return (LOCAL::PERCORSO_AUDIO + "inizio.mp3"); break;
        case tipoAudio::SWAP:
            return (LOCAL::PERCORSO_AUDIO + "swap.mp3"); break;
        case tipoAudio::ERRORE:
            return (LOCAL::PERCORSO_AUDIO + "errore.mp3"); break;
        default:
            std::cerr << "Bad call of get_audio_url with unknown audio type" << std::endl;
            return "none.mp3";
    }
}

void MainWindow::Messaggio::print() const {
    if (tipo == Tipo::error) {
        std::cerr << messaggioCerr << std::endl; return;
    }
    else if (tipo == Tipo::info) {
        std::cout << messaggioCerr << std::endl;
    }
}

MainWindow::Messaggio::Messaggio(CommonMessage m, Partita* p){
    partita = p;
    switch (m) {
    case CommonMessage::FineSuccess:
        messaggioGUI = std::string("Partita finita con successo. Ora kicka i perdenti.");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + " finita con successo.");
        tipo = Messaggio::Tipo::info;
        break;
    case CommonMessage::GoalSuccess:
        messaggioGUI = std::string("Goal aggiunto con successo.");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + " goal aggiunto");
        tipo = Messaggio::Tipo::info;
        break;
    case CommonMessage::InitSuccess:
        messaggioGUI = std::string("Partita iniziata con successo.");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + " iniziata");
        tipo = Messaggio::Tipo::info;
        break;
    case CommonMessage::NoAnswer:
        messaggioGUI = std::string("Il server non ha risposto. Sei connesso alla rete?");
        messaggioCerr = std::string("Nessuna risposta dal server. Controlla la connessione");
        tipo = Messaggio::Tipo::error;
        break;
    case CommonMessage::NoArd:
        messaggioGUI = std::string("Arduino non collegato.");
        messaggioCerr = std::string("Arduino non collegato");
        tipo = Messaggio::Tipo::error;
        break;
    case CommonMessage::PermDenied:
        messaggioGUI = std::string("Risposta inaspettata dal server");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + "Permission denied: ");
        tipo = Messaggio::Tipo::error;
        break;
    case CommonMessage::SwapSuccess:
        messaggioGUI  = std::string("Swap avvenuto con successo");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + " Swap corretto");
        tipo = Messaggio::Tipo::info;
        break;
    case CommonMessage::AnnSuccess:
        messaggioGUI = std::string("Il goal è stato annullato con successo");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + " goal annullato con successo");
        tipo = Messaggio::Tipo::info;
        break;
    case CommonMessage::AnnullamentoAzione:
        messaggioGUI = std::string("Il server ha risposto in modo inaspettato, l'ultima azione è stata annullata.");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + " ultima azione annullata");
        tipo = Messaggio::Tipo::error;
        break;
    }
}

MainWindow::Messaggio::Messaggio(CommonMessage m, Partita::Squadra s, Partita* p){
    partita = p;
    std::string squadra_fullname = s == Partita::Squadra::Red ? std::string("rossi") : std::string("blu");
    switch (m) {
    case CommonMessage::GoalSuccess:
        messaggioGUI = std::string("Goal aggiunto con successo ai " + squadra_fullname + ".");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + " goal " + squadra_fullname + " aggiunto");
        tipo = Messaggio::Tipo::info;
        break;
    case CommonMessage::SwapSuccess:
        messaggioGUI  = std::string("Swap dei " + squadra_fullname + " avvenuto con successo");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + " swap " + squadra_fullname + " corretto");
        tipo = Messaggio::Tipo::info;
        break;
    case CommonMessage::AnnSuccess:
        messaggioGUI = std::string("Goal annullato con successo ai " + squadra_fullname + ".");
        messaggioCerr = std::string("Partita " + std::to_string(partita->get_pk()) + " goal " + squadra_fullname + " annullato con successo");
        tipo = Messaggio::Tipo::info;
        break;
    default:
        std::cerr << "Bad creation of CommonMessage: squadra passed, but not required" << std::endl;
        MainWindow::Messaggio(m, p);
    }
}

MainWindow::Messaggio::Messaggio(CommonMessage m, Partita *p, std::exception &e){
    MainWindow::Messaggio(m, p);
    messaggioCerr += e.what();
}



void MainWindow::aggiornaPlayer(const Giocatore g, Partita::Posizione p){
  partita.get_giocatori()[p] = g;
  aggiornaNomiFoto();
}
