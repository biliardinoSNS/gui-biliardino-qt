
/**
 * @file controllasigarduino.h
 * @brief Thread per controllare i messaggi in arrivo da Arduino
 */



#ifndef CONTROLLASIGARDUINO_H
#define CONTROLLASIGARDUINO_H

#include <QObject>
#include <QThread>
#include <QTime>

#include "giocatore.h"
#include "partita.h"
#include "arduino.h"
#include "time_delta.h"



/**
 * @brief Thread che controlla i messaggi in arrivo via seriale sulla ttyACM0 e li comunica alla MainWindow
 */

class controllaSigArduino : public QThread {
    Q_OBJECT
public:
    controllaSigArduino(QObject* parent, Arduino* ard = nullptr);
    void run();
    void stop() { continua = false; quit(); exit(); }

    /**
     * @brief Enorme simil-switch terribilmente hardcodato per smistare i messaggi possibili.
     */
    void gestisciMessaggio(std::string);

    /**
     * @brief Generica eccezione da lanciare quando arriva un messaggio distorto da Arduino
     */
    class badMessage : public std::exception {
    public:
        badMessage(std::string m) { messaggio = m; }
        virtual const char* what() const throw() { std::cerr << messaggio << std::endl; return "Messaggio da arduino insensato."; }
    private: std::string messaggio;
    };



signals:
    void sigRicollega();
    void sigEvento(Partita::tipoEvento e, Partita::Squadra squadra);
    void sigEvento(Partita::EventoSquadra e);
public slots:

  /**
   * @brief Quando Arduino non è disponibile, questo slot tenta il ricollegamento
   */
  void prendiArd(Arduino*);
  /**
   * @brief Quando si vuole vedere il plot, è necessario fermare questo thread, altrimenti confliggono
   */
  void ferma() { return stop(); }

private:
    bool continua;
    Arduino* arduino;
    QTime tempoUltimoComando;
};



#endif // CONTROLLASIGARDUINO_H
