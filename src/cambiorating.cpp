#include "cambiorating.h"
#include "ui_cambiorating.h"

CambioRating::CambioRating(Partita p, QObject* appo, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CambioRating)
{
    ui->setupUi(this);
    partita = p;
    qRegisterMetaType<Giocatore>("Giocatore");
    qRegisterMetaType<Partita::Posizione>("Partita::Posizione");
    connect(this, SIGNAL(mandaGiocatore(const Giocatore, const Partita::Posizione)), appo->parent(), SLOT(aggiornaPlayer(const Giocatore&, Partita::Posizione)));
    
    for (auto it = partita.get_giocatori().begin(); it != partita.get_giocatori().end(); it++){
        QLabel* lab = get_label(it->first);
        QLabel* foto = get_foto(it->first);
	emit mandaGiocatore(it->second, it->first);
        std::string testo = it->second.get_nome() + "\nDef: " + \
                it->second.get_old_rating_def() + " => " + it->second.get_rating_def() + "\nAtk: " + \
                it->second.get_old_rating_atk() + " => " + it->second.get_rating_atk();
        lab->setText(QString::fromStdString(testo));
        foto->setPixmap(QPixmap(QString::fromStdString(Giocatore::PERCORSO_FOTO + it->second.get_foto())).scaledToWidth(200));
    }
}

CambioRating::~CambioRating()
{
    delete ui;
}


QLabel* CambioRating::get_label(const Partita::Posizione &p) {
  switch (p.get_ruolo()) {
    case Giocatore::Ruolo::Attacco:
      if (p.get_squadra() == Partita::Squadra::Red) return ui->testoAR;
      else return ui->testoAB;
      break;
    case Giocatore::Ruolo::Difesa:
      if (p.get_squadra() == Partita::Squadra::Red) return ui->testoDR;
      else return ui->testoDB;
      break;
    default:
      std::cerr << "CambioRating::get_label called with wrong parameter" << std::endl;
      break;
    }
    throw std::exception();
}


QLabel* CambioRating::get_foto(const Partita::Posizione &p){
  switch (p.get_ruolo()) {
    case Giocatore::Ruolo::Attacco:
      if (p.get_squadra() == Partita::Squadra::Red) return ui->fotoAR;
      else return ui->fotoAB;
      break;
    case Giocatore::Ruolo::Difesa:
      if (p.get_squadra() == Partita::Squadra::Red) return ui->fotoDR;
      else return ui->fotoDB;
      break;
    default:
      std::cerr << "CambioRating::get_foto called with wrong parameter" << std::endl;
      break;
    }
    throw std::exception();
}

