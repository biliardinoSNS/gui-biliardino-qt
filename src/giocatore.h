/**
 * @file giocatore.h
 * @brief Classe Giocatore, fondamentale per tutto il resto del progetto.
 */



#ifndef GIOCATORE_H
#define GIOCATORE_H

#include <iostream>
#include <sstream>

#include <string>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>

#include "local_settings.h"



/**
 * @brief Classe Giocatore.
 * Classe che viene utilizzata da Partita. Include informazioni fondamentali sul giocatore come il nome, il barcode, la sua primary key, la posizione nel filesystem della sua foto e l'url da cui scaricarla.
 */

class Giocatore
{
public:

  /**
   * @brief Sottoclasse di Giocatore che include un dato con media e deviazione standard, corrispondente a quello del webserver.
   */
  class Rating {
    public:
        Rating() { media = 0; sdev = 0; }
        Rating(int m, int s) { media = m; sdev = s; }
        Rating(const Rating& rhs) { media = rhs.media; sdev = rhs.sdev; }
        std::string show_html() const { return std::to_string(media) + " ± " + std::to_string(sdev); }

        friend std::ostream& operator<< (std::ostream& os, const Rating& r){ return os << r.show_html(); }

    private:
        int media;
        int sdev;
    };
    enum class Ruolo { Attacco, Difesa };


    /**
     * @brief Costruttore di default che resituisce un giocatore equivalente a Giocatore::nessuno
     */
    Giocatore();
    /**
     * @brief Costruttore quando si ha solo nome, cognome, barcode e pk
     */
    Giocatore(std::string n, std::string bar, int p);
    Giocatore(std::string n, std::string bar, std::string ur, std::string f, int p);
    Giocatore(std::string n, std::string bar,
              std::string ur, std::string f, int p, Rating d, Rating a);

    
    /**
     * @brief Ricostruisce il giocatore a partire da un dato in Json
     * Il server restituisce le informazioni in formato Json.
     * @param v Il dato json in formato Json::Value, preso dal server
     * @param bar Il server non restituisce il valore del barcode perché sarebbe ridondante, ma ci fa comunque piacere memorizzarlo. Di conseguenza viene inserito a mano nel costruttore.
     */
    Giocatore(Json::Value, std::string bar);
    Giocatore(const Giocatore& rhs);

    std::string get_nome() const { return nome; }
    std::string get_barcode() const { return barcode; }
    std::string get_foto() const { return foto; }
    unsigned int get_pk() const { return pk; }
    std::string get_urlFoto() const { return urlFoto; }
    std::string get_rating(Ruolo r) { Rating appo = rating.at(r); return appo.show_html(); }
    std::string get_old_rating(Ruolo r) { Rating appo = oldRating.at(r); return appo.show_html(); }
    inline std::string get_rating_atk() { return get_rating(Ruolo::Attacco); }
    inline std::string get_rating_def() { return get_rating(Ruolo::Difesa); }
    inline std::string get_old_rating_def() { return get_old_rating(Ruolo::Difesa); }
    inline std::string get_old_rating_atk() { return get_old_rating(Ruolo::Attacco); }

    void set_foto(std::string s) { foto = s; }
    void set_rating(Ruolo ru, Rating ra) { rating[ru] = ra; }
    void copy_old_rating() { oldRating = rating; }


    /**
     * @brief I giocatori sono confrontati solo in base al pk.
     */
    bool operator==(const Giocatore& g) const { return pk == g.pk; }

    /**
     * @brief Informazione presa dalle configurazioni locali (si veda local_settings.h). È la stringa a cui appendere solo il nome tipo "5.jpg".
     */
    static const std::string BASE_URL_FOTO;
    /**
     * @brief La posizione nel filesystem della cartella con le foto. Esempio "/usr/shared/biliardino/media/".
     */
    static std::string PERCORSO_FOTO;
    /**
     * @brief Giocatore jolly da utilizzare quando i giocatori non ci sono o quando viene richiesto un giocatore inesistente.
     */
    static const Giocatore nessuno;
    /**
     * @brief Chiamata all'inizializzazione della MainWindow, serve ad inizializzare nel posto giusto questa variabile.
     */
    static void set_foto_dir() { Giocatore::PERCORSO_FOTO = LOCAL::PERCORSO_FOTO;  }


      /**
       * @brief Utile per il debug, non viene mai utilizzata
       */
      friend std::ostream& operator<< (std::ostream& os, Giocatore& g){
        return os << g.get_nome() + "\t" + g.get_foto() + "\tBarcode: " + \
                     g.get_barcode() + "\tpk: " << g.get_pk() << "\nrating_atk" \
                  << g.get_rating_atk() << "\trating_def:" << g.get_rating_def() \
                  << "\noldRating" << g.get_old_rating_atk() << "\t" << g.get_old_rating_def();
    }

private:
    std::string nome; /**< Nome e cognome separati da uno spazio */
    std::string foto; /**< Percorso relativo alla cartella dove ci sono tutte le foto. E.g. "5.jpg". */
    std::string urlFoto; /**< Url relativo da cui scaricare la foto */
    unsigned int pk; /**< Si commenta da solo */
    std::string barcode; /**< Si commenta da solo */
    /**
     * Il rating di una persona è diverso per attacco e difesa. 
     * Dato che quando qualcuno viene swappato il rating deve essere diverso, ha senso memorizzare subito entrambe.
     */
    std::map<Ruolo, Rating> rating;

    /**
     * Utile quando bisogna mostrare il cambiamento del rating fra prima e dopo.
     */
    std::map<Ruolo, Rating> oldRating;
};

#endif // GIOCATORE_H
