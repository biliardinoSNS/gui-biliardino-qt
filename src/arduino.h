
/**
 * @file arduino.h
 * @brief Definzione della classe Arduino che serve per la comunicazione seriale
 */


#ifndef ARDUINO_H
#define ARDUINO_H

#include <string>
#include <exception>

#include <QtSerialPort/QSerialPort>
#include <QTextStream>
#include <QTimer>
#include <QByteArray>
#include <QObject>
#include <QCoreApplication>



#include "utils.h"

/**
 * @brief Converte una std::string in un QByteArray
 */
QByteArray strToB(std::string s);

/**
 * @brief Converte un QByteArray in una std::string
 */
std::string bToStr(QByteArray b);


/**
 * @brief Classe che comanda l'interazione con arduino via seriale.
 * Permette di segnare goal, ricevere informazioni come i grafici, settare i parametri delle sigma di calibrazione.
 */

class Arduino {
public:
    Arduino();
    ~Arduino();

    enum class Porte { Red, Blu, SRed, SBlu, Tutte }; /**< Porte di cui chiedere il grafico */

    /**
     * Scrive sulla seriale di arduino i punteggi
     * @param pr punteggio rossi
     * @param pb punteggio blu
     */
    inline void scriviPunteggio(unsigned int pr, unsigned int pb ) { scrivi("gr" + std::to_string(pr) + "b" + std::to_string(pb) + "e"); }

    /**
     * Legge dalle variabili statiche Arduino::sigma di questa classe per modificare il valore di sigma.
     * @param super Se il biliardino ha i supergoal o no
     */
    void cambiaSigma(bool super);
    
    inline void ardFine(std::string vincitore) { scrivi("f" + vincitore); } /**< Dice ad arduino che la partita è finita */
    inline void ardInizio() { scrivi("i"); } /**< Dice ad arduino che la partita è iniziata */
    inline void calibra() { scrivi("calibra"); } /**< Chiede ad arduino di eseguire da solo la calibrazione delle fotocellule */
    void chiediPlot(Porte p); /**< Chiede ad arduino di inviare i dati in tempo reale dalla porta @param p la porta che si vuole */
    inline void chiediPlot(std::string s) { return scrivi("debug" + s);} /**< Chiede ad arduino di inviare i dati in tempo reale da tutte le porte */
    inline void stopPlot() { return scrivi("s"); } /**< Chiede ad arduino di fermare l'invio dei dati dalla porta e riprende il normale funzionamento */
    qint64 available() const; /**< La porta è disponibile? */
    inline bool canReadLine() const { return porta.canReadLine(); } /**< Ci sono dati disponibili sulla porta? */
    inline QByteArray readAllBytes() { return porta.readAll(); } /**< Legge tutto quello che arduino ha da dire sulla seriale e lo spara in un QByteArray */
    inline std::string readAll() { return bToStr(readAllBytes()); }  /**< Legge tutto dalla seriale ma lo spara in una std::string  */
    std::string readLine(); /**< Legge una riga dalla porta seriale */
    inline bool readChar(char* c) { return porta.getChar(c); } /**< Legge un solo carattere dalla porta seriale */
    void scrivi(std::string s); /**< Scrive sulla porta seriale */
    inline bool esiste() { return (porta.isOpen() && porta.isWritable());  /**< Controlla se arduino è collegato. */}

    
    void reset();  /**< Prova a ricollegarsi ad Arduino provando la ACM0 e ACM1 */
    void inizializza(); /**< Setta le porte e si prepara a comunicare. Viene chiamato dal costruttore e da reset() */


    /**
     * @brief Eccezione lanciata quando arduino non viene trovato
     */
    class noArd: public std::exception {
    public:  virtual const char* what() const throw() { return "Arduino non collegato!"; }
    };
    /**
     * @brief Eccezione lanciata quando la porta seriale risponde male
     */
    class erroreLettura: public std::exception {
    public:  virtual const char* what() const throw() { return "Errore di lettura dalla porta seriale."; }
    };

    
    static std::map<Arduino::Porte, double> sigma; /**< Il valore di calibrazione delle porte di arduino */
    /**
     * @brief Cambia le variabili statiche Arduino::sigma ma non lo comunica ad arduino. Si veda cambiaSigma() per la comunicazione. Senza supergoal 
     * @param r la nuova sigma dei goal rossi MOLTIPLICATA PER 10 
     * @param b la nuova sigma dei goal blu MOLTIPLICATA PER 10
     * @warning Ricordarsi che le sigma vanno moltiplicate per 10, come indicato nella descrizione dei parametri
     */
    static void editSigma(unsigned int r, unsigned int b); 

    /**
     * @brief Cambia le variabili statiche Arduino::sigma ma non lo comunica ad arduino. Si veda cambiaSigma() per la comunicazione. Con supergoal 
     * @param r la nuova sigma dei goal rossi MOLTIPLICATA PER 10 
     * @param b la nuova sigma dei goal blu MOLTIPLICATA PER 10
     * @param sr la nuova sigma del supergoal rosso MOLTIPLICATA PER 10
     * @param sb la nuova sigma del supergoal blu MOLTIPLICATA PER 10
     * @warning Ricordarsi che le sigma vanno moltiplicate per 10, come indicato nella descrizione dei parametri
     */
    static void editSigma(unsigned int r, unsigned int b, unsigned int sr, unsigned int sb); 
    
    /**
     * @brief Cambia le variabili statiche Arduino::sigma ma non lo comunica ad arduino. Si veda cambiaSigma() per la comunicazione. Con supergoal 
     * @param r la nuova sigma dei goal rossi
     * @param b la nuova sigma dei goal blu
     * @param sr la nuova sigma del supergoal rosso
     * @param sb la nuova sigma del supergoal blu
     */
    static void editSigma(double, double,double, double);
    
private:
    QSerialPort porta; /**< L'oggetto su cui scrivere e comunicare */
};





#endif // ARDUINO_H
