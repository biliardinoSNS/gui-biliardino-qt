/**
 * @file mainwindow.h
 * @brief Finestra principale da cui fare tutto il resto.
 */


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QLCDNumber>
#include <QThread>
#include <QtSerialPort/QSerialPort>
#include <QCoreApplication>
#include <QCloseEvent>
#include <QTime>
#include <QMediaPlayer>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>
#include <string>
#include <map>
#include <algorithm>
#include <list>
#include <queue>

#include "giocatore.h"
#include "partita.h"
#include "inseriscinuovacoda.h"
#include "finepartita.h"
#include "serverrequests.h"
#include "arduino.h"
#include "plot.h"
#include "cambiasigma.h"
#include "guida.h"
#include "runguard.h"
#include "evento.h"
#include "attendirisposta.h"
#include "controllasigarduino.h"





namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


    /**
     * @brief Macro per mettere le foto dei giocatori al posto dei QLabel
     */
    void setPixmapFromPercorso(std::string percorso, QLabel* bersaglio);
    void aggiornaNomiFoto(); /**< Legge le informazioni dentro la variabile this->partita e aggiorna i nomi e le foto di conseguenza. */
    void aggiornaPunteggi() { return aggiornaPunteggi(true);}
    /**
     * @brief Aggiorna solo i punteggi sulla GUI. 
     * @param avvertiArd Comunica anche ad Arduino il cambiamento dei punteggi o meno.
     */
    void aggiornaPunteggi(bool avvertiArd);

    void set_inCorso(bool b) { inCorso = b; }
    void eliminaCoda(int i) { auto it = coda.begin(); std::advance(it, i); coda.erase(it); }
    void collegaArduino();
    void menuSupergoal();


    /**
     * @brief Utile per la gestione dei log contemporanei sullo stdout, stderr e sulla console di log della GUI.
     */
    class Messaggio {
    public:

        enum class Tipo { info, warn, error }; /**< Tipo di log message. */
        enum class CommonMessage { NoAnswer, PermDenied, NoArd, GoalSuccess, InitSuccess, FineSuccess, SwapSuccess, AnnSuccess, AnnullamentoAzione };
        Messaggio(std::string m1, Partita* p, std::string m2 = "", Tipo t = Tipo::info) { messaggioGUI = m1; messaggioCerr = m2; tipo = t; partita = p; }
        Messaggio(std::string m, Partita* p, std::exception& e, Tipo t = Tipo::error) { messaggioGUI = m; tipo = t; messaggioCerr = e.what(); partita = p; }
        Messaggio(std::string m, Partita* p, Tipo t) { messaggioGUI = m; tipo = t; partita = p; }
        Messaggio(CommonMessage m, Partita* p);
        Messaggio(CommonMessage m, Partita::Squadra s, Partita* p);
        Messaggio(CommonMessage m, Partita* p, std::exception& e);
        QString msgLog() const;
        std::string get_msg_cerr() const { return messaggioCerr; }
        void print() const;
    private:
        Partita* partita; /**< Serve per ottenere informazioni sulla partita a cui si riferisce */
        Tipo tipo; /**< Autoesplicativo */
        std::string messaggioGUI; /**< Il messaggio da riportare sulla GUI. */
        std::string messaggioCerr; /**< Il messaggio da riportare sullo stdout o sullo stderr. Il nome può essere fuorviante. */
    };


    void printLogger(Messaggio m);
    inline void printLogger(std::string s, std::exception& e) { return printLogger(Messaggio(s, &partita, e));}
    inline void printLogger(std::string s, Messaggio::Tipo t) { return printLogger(Messaggio(s, &partita, t));}
    inline void printLogger(char* s, std::exception& e) { return printLogger(std::string(s), e); }
    inline void printLogger(char* s, Messaggio::Tipo t) { return printLogger(std::string(s), t); }
    inline void printLogger(char* s) { return printLogger(Messaggio(std::string(s), &partita));}
    inline void printLogger(std::string s) { return printLogger(Messaggio(s, &partita));}
    inline void printLogger(Messaggio::CommonMessage m) { return printLogger(Messaggio(m, &partita)); }
    inline void printLogger(Messaggio::CommonMessage m, Partita::Squadra squadra) { return printLogger(Messaggio(m, squadra, &partita)); }
    inline void printLogger(Messaggio::CommonMessage m, std::exception& e) { return printLogger(Messaggio(m, &partita, e)); }


    void punti(Partita::Squadra, int); /**< Scrivi il nuovo punteggio di una squadra e aggiornalo. */

    /**
     * @brief Fa tutto quello che serve per segnare un goal e dirlo al server.
     * Controlla se c'è una partita in corso, manda il goal al server facendo partire il thread, poi controlla se si è raggiunto il numero limite di goal e in caso chiama il dialog FinePartita. Ovviamente aggiorna anche i punteggi sulla MainWindow.
     * @param s La squadra a cui aggiungere il goal
     * @param tipo Il tipo di goal fatto. In effetti bisognerebbe togliere questo hardcoding prima o poi.
     */
    void addGoal(Partita::Squadra s, std::string tipo);

    /**
     * @brief Fa tutto quello che serve per annullare un goal.
     * @param s La squadra a cui toglierlo.
     */
    void togliGoal(Partita::Squadra s);
    void kick(Partita::Squadra); /**< Toglie i giocatori che stanno attualmente nella squadra s. */
    void togliCoda(); /**< Toglie le persone della coda attualmente selezionate. */
    void scalaCoda(Partita::Squadra s); /**< Autoesplicativo. */
    void eseguiSwap(Partita::Squadra s); /**< Swappa le persone che stanno giocando nella squadra s. */
    void iniziaPartita(); /**< Fa tutto quello che serve per iniziare una partita */
    void finisciPartita();
    void finisciPartitaKick();
    void mettiInGioco(Giocatore& atk, Giocatore& def, Partita::Squadra s);

    /**
     * @brief Chiede il grafico dalle porte analogiche di Arduino.
     * Ferma il thread che legge i messaggi in arrivo e apre un dialog Plot che mostra quello che si vede sulla porta p.
     * @param p La porta di cui si vuole il grafico.
     */
    void plot(Arduino::Porte p);
    void kickECoda(Partita::Squadra s); /**< Kicka la squadra s e la rimette in coda */
    void muoviCoda(int dir);
    void changeButtonPalette(bool b); /**< Quando è true i bottoni diventano verdi, false tornano come prima */

    enum class tipoAudio { INIZIO, ALERTFINE, FINE, SWAP, ERRORE }; /**< Lista degli audio riproducibili. */
    std::string get_audio_url(tipoAudio t); /**< Restituisce la posizione nel filesystem dell'audio richiesto. */
    void riproduciAudio(tipoAudio t); /**< Riproduce l'audio richiesto. */



    /**
     * @brief Utilizzata per mettere in coda la gente.
     */
    class Coppia {
    public:
        Coppia() { atk = Giocatore::nessuno; def = Giocatore::nessuno; }
        Coppia(Giocatore& a, Giocatore& d) { atk = a; def = d; }

        std::string nome() const { return atk.get_nome() + ", " + def.get_nome(); }
	
        Giocatore atk;
        Giocatore def;
    };


    QLabel* get_label(const std::string) const; /**< Restituisce la label su cui mettere il nome del giocatore a partire dalla stringa a cui corrisponde (AR, AB, DR, DB). */
    QLabel* get_label(const Partita::Posizione& p) const; /**< Restituisce la label su cui mettere il nome del giocatore a partire dalla posizione a cui corrisponde. */
    QLabel* get_foto(const std::string) const; /**< Restituisce la label su cui mettere la foto del giocatore a partire dalla stringa a cui corrisponde (AR, AB, DR, DB). */
    QLabel* get_foto(const Partita::Posizione &p) const; /**< Restituisce la label su cui mettere la foto del giocatore a partire dalla posizione. */
    QLCDNumber* get_LCD(const std::string) const; /**< Restituisce l'LCD dei rossi o dei blu */
    QLCDNumber* get_LCD(const Partita::Squadra&) const; /**< Restituisce l'LCD dei rossi o dei blu */

    std::list<Coppia>& get_coda() { return coda; } /**< La coda */
    bool get_inCorso() { return inCorso; }

    void closeEvent(QCloseEvent *event); /**< Bisogna terminare il thread che legge la seriale per evitare memory leak. */
    void attendiRisposta(Evento* ev); /**< Lancia un thread che aspetta la risposta dal web server e in caso vada male annulla l'ultima operazione fatta basandosi sul tipo di ev.*/

signals:
    void nuovoArd(Arduino*);
    void fermaThread();

public slots:
    void addCoda(Giocatore &, Giocatore &);
    void finisci() { finisciPartita(); }
    void finisciKick() { finisciPartitaKick(); }
    void annullaFineAutomatica();
    void cambiaSigma();
    void ricollegaArd();
    void eventoArduino(Partita::EventoSquadra);
    void login() { Server::login(Server::cookieList); }
    void gestisciErroreServer(Evento* ev);
    void stopPlot();
    void aggiornaPlayer(const Giocatore, Partita::Posizione);

    
private slots:
    void on_btnIniziaPartita_clicked();
    void on_btnFinePartita_clicked() { finisciPartita(); }
    void on_btnSwapRed_clicked() { eseguiSwap(Partita::Squadra::Red); }
    void on_btnAddGoalRed_clicked() { addGoal(Partita::Squadra::Red, "GP");}
    void on_btnTogliGoalRed_clicked() { togliGoal(Partita::Squadra::Red); }
    void on_btnSwapBlu_clicked() { eseguiSwap(Partita::Squadra::Blu); }
    void on_btnAddGoalBlu_clicked() { addGoal(Partita::Squadra::Blu, "GP");}
    void on_btnTogliGoalBlu_clicked() { togliGoal(Partita::Squadra::Blu);}
    void on_btnAddCoda_clicked();
    void on_btnScalaCodaRed_clicked() { scalaCoda(Partita::Squadra::Red); }
    void on_btnScalaCodaBlu_clicked() { scalaCoda(Partita::Squadra::Blu); }
    void on_btnTogliCoda_clicked() { togliCoda(); }
    void on_btnKickRed_clicked();
    void on_btnKickBlu_clicked();
    void on_btnKickECodaRed_clicked();
    void on_btnKickECodaBlu_clicked();
    void on_btnCambioCampo_clicked();
    void on_btnCodaSu_clicked();
    void on_btnCodaGiu_clicked();


    void on_actionSupergoal_rossa_triggered() { return plot(Arduino::Porte::Red); }
    void on_actionSupergoal_blu_triggered() { return plot(Arduino::Porte::Blu); }
    void on_toolbarGraficoRed_triggered() { return plot(Arduino::Porte::SRed); }
    void on_toolbarGraficoBlu_triggered() { return plot(Arduino::Porte::SBlu); }
    void on_actionTutte_triggered() { return plot(Arduino::Porte::Tutte); }

    void on_toolbarCalibra_triggered();
    void on_toolbarCambiaSigma_triggered();
    void on_toolbarRicollegaArduino_triggered();
    void on_actionGuida_triggered();

private:
    Ui::MainWindow *ui;
    controllaSigArduino* checkArdu;


    std::queue<AttendiRisposta*> eventi;
    Arduino arduino;
    Partita partita; /**< La partita attualmente in corso. */
    Partita partitaDaRecuperare; /**< Nel momento in cui la partita viene finita, la variabile partita viene resettata. Se il server non riesce a terminare la partita bisogna recuperare le informazioni da qualche parte. Questo è il dove. */
    bool inCorso; /**< Ci sono delle partite in corso? */
    std::list<Coppia> coda;
    bool supergoal; /**< Il biliardino ha i supergoal? */
};

#endif // MAINWINDOW_H
