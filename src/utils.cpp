#include "utils.h"


int fromStr(std::string s){
    std::stringstream appo;
    appo << s;
    int ret;
    appo >> ret;
    return ret;
}


std::string decode(const std::string& input)
{
  // choose a power of two => then compiler can replace "modulo x" by much faster "and (x-1)"
  const size_t passwordLength = 16;
  // at least as long as passwordLength, can be longer, too ...
  static const char password[23] = "Arduino non collegato!";
  // out = in XOR NOT(password)
  std::string result = input;
  for (size_t i = 0; i < input.length(); i++)
    result[i] ^= ~password[i % passwordLength];
  return result;
}

std::string get_env_or_default(const std::string& variable_name, const std::string& default_value)
{
    const char* value = getenv(variable_name.c_str());
    return value ? value : default_value;
}


void jsonFromStr(Json::Value& root, std::string s){
    Json::Reader reader;
    bool parsingSuccessful = reader.parse( s.c_str(), root );     //parse process
    if ( !parsingSuccessful )    {
        throw std::exception();
    }
}
