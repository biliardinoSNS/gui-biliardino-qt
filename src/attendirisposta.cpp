#include "attendirisposta.h"

AttendiRisposta::AttendiRisposta(QObject *parent, Evento *ev) : QThread(parent){
    connect(this, SIGNAL(sigError(Evento*)), parent, SLOT(gestisciErroreServer(Evento*)));
    connect(this, SIGNAL(sigLogin()), parent, SLOT(login()));
    evento = ev;
}


AttendiRisposta::~AttendiRisposta(){
    delete evento;
}



void AttendiRisposta::run(){
    using namespace Server;
    using namespace std;
    bool success = false;
    int retries = 0;
    while((!success) && (retries < 3)){
        try {
            mandaRichiestaServer(evento);
            success = true;
            if (evento->tipoEvento() == Evento::tipiEvento::fine){
                CambioRating* cambioRating = new CambioRating(static_cast<Fine*>(evento)->get_partita(), this);
                cambioRating->show();
            }
        }
        catch (Server::erroreServer& e){
            cerr << e.what() << endl;
        }
        catch (Server::permissionDenied& e){
            cerr << e.what() << endl;
            emit sigLogin();
            QThread::sleep(1);
        }
        catch (curlpp::LogicError& e){
            cerr << "Errore logico: " << e.what() << endl;
        }
        catch (Server::erroreRichiesta& e){
            cerr << "Errore richiesta: " << e.what() << endl;
        }
        catch (badAnswer& e){
            cerr << e.what() << endl;
        }
        catch (Inizio::playerOccupato& e){
            cerr << e.what() << endl;
            break;
        }
        catch (...){
            cerr << "Errore non previsto!" << endl;
        }
        retries++;
    }
    if (!success) {
        emit sigError(evento);
    }
    stop();
}

