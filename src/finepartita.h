/**
 * @file finepartita.h 
 * @brief Definizione di una classe che è una finestra di dialogo per finire automaticamente 
 * la partita al raggiungimento di un numero di goal fissato nel config
 */



#ifndef FINEPARTITA_H
#define FINEPARTITA_H

#include <QDialog>
#include <QThread>
#include <string>
#include <sstream>

#include <partita.h>


/**
 * @brief Conto alla rovescia per finire automaticamente la partita
 */
class ContoAllaRovescia : public QThread {
    Q_OBJECT

public:

    static const unsigned int DEFAULT_TIMEOUT_S;

    ContoAllaRovescia(QObject* parent) : QThread(parent) { continua = true; tempo = DEFAULT_TIMEOUT_S; }
    void run() { while  (continua) { emit set_messaggio( tempo );  sleep(1); if (tempo > 1) tempo -= 1; else emit ok(); } }
    void stop() { continua = false; quit(); exit(); }

    bool get_continua() const { return continua; }
    unsigned int get_tempo() const { return tempo; }

signals:
    void set_messaggio(unsigned int i);
    void ok();

private:
    bool continua;
    unsigned int tempo;
};



/**
 * @brief Finestra di dialogo con conto alla rovescia e 3 bottoni
 */
namespace Ui {
class FinePartita;
}

class FinePartita : public QDialog
{
    Q_OBJECT


public:
    explicit FinePartita(QWidget *parent = 0);
    ~FinePartita();

signals:
    void sigOk();
    void sigOkKick();
    void sigAnnulla();

public slots:
    void set_messaggio(unsigned int i);
    void close_se_goal(Partita::EventoSquadra);

private slots:
    void on_btnAnnulla_clicked();
    void on_btnOk_clicked();
    void on_btnOkKick_clicked();

private:
    Ui::FinePartita *ui;
    ContoAllaRovescia* countdown;
};


#endif // FINEPARTITA_H
