#include "arduino.h"


QByteArray strToB(std::string s) { QByteArray byteArray(s.c_str(), s.length()); return byteArray; }
std::string bToStr(QByteArray b) { std::string s(b.constData(), b.length()); return s;}


std::map<Arduino::Porte, double> Arduino::sigma;


Arduino::Arduino(){
    inizializza();
}

void Arduino::reset(){
    inizializza();
}


void Arduino::inizializza(){
    porta.setPortName("ttyACM0");
    porta.open(QIODevice::ReadWrite);
    porta.setBaudRate(QSerialPort::Baud9600);
    porta.setDataBits(QSerialPort::Data8);
    porta.setParity(QSerialPort::NoParity);
    porta.setStopBits(QSerialPort::OneStop);
    porta.setFlowControl(QSerialPort::NoFlowControl);
    if (esiste()) return;
    std::cerr << "No Arduino found on port ttyACM0" << std::endl;
    porta.setPortName("ttyACM1");
    porta.open(QIODevice::ReadWrite);
    porta.setBaudRate(QSerialPort::Baud9600);
    porta.setDataBits(QSerialPort::Data8);
    porta.setParity(QSerialPort::NoParity);
    porta.setStopBits(QSerialPort::OneStop);
    porta.setFlowControl(QSerialPort::NoFlowControl);
    if (!esiste())
      std::cerr << "No Arduino found on port ttyACM1" << std::endl; 
}



Arduino::~Arduino(){
    porta.close();
}

void Arduino::scrivi(std::string s) {
    if (porta.isOpen() && porta.isWritable())    {
        porta.write(strToB(s));
        porta.flush();
    }
    else{
        throw Arduino::noArd();
    }
}

qint64 Arduino::available() const {
    if (porta.isOpen() && porta.isWritable())    {
        return porta.bytesAvailable();
    }
    else
        throw Arduino::noArd();
}


void Arduino::editSigma(unsigned int r, unsigned int b){
    double rr, bb;
    rr = ((double) r)/10;
    bb = ((double) b)/10;
    Arduino::sigma[Porte::Red] = rr;
    Arduino::sigma[Porte::Blu] = bb;
}

void Arduino::editSigma(double r, double b, double sr, double sb){
    Arduino::sigma[Porte::Red] = r;
    Arduino::sigma[Porte::Blu] = b;
    Arduino::sigma[Porte::SRed] = sr;
    Arduino::sigma[Porte::SBlu] = sb;
}



void Arduino::cambiaSigma(bool super){
    unsigned int r = (int) (Arduino::sigma[Porte::Red] * 10);
    unsigned int b = (int) (Arduino::sigma[Porte::Blu] * 10);
    if (super){
        unsigned int sr = (int) (Arduino::sigma[Porte::SRed] * 10);
        unsigned int sb = (int) (Arduino::sigma[Porte::SBlu] * 10);
        scrivi("sr" + std::to_string(r) + "b" + std::to_string(b) + "R" + std::to_string(sr) + "B" + std::to_string(sb) + "e");
    }
    else{
        scrivi("sr" + std::to_string(r) + "b" + std::to_string(b) + "e");
    }
}


void Arduino::chiediPlot(Porte p){
    switch (p) {
    case Porte::Red: scrivi("debugr"); break;
    case Porte::Blu: scrivi("debugb"); break;
    case Porte::SRed: scrivi("debugR"); break;
    case Porte::SBlu: scrivi("debugB"); break;
    case Porte::Tutte: scrivi("dEBUG"); break;
    }
}


std::string Arduino::readLine(){
    QByteArray appo = porta.readLine(20);
    std::string ret = bToStr(appo);
    return ret;
}
