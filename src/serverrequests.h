/**
 * @file serverrequests.h
 * @brief Contiene le API per comunicare con il server web.
 * Ho utilizzato ampiamente la libreria curlpp per gestire le richieste http in c++.
 */



#ifndef SERVERREQUESTS_H
#define SERVERREQUESTS_H



#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Infos.hpp>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <exception>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/writer.h>


#include "partita.h"
#include "evento.h"



/**
 * @brief Namespace con tutte le API per comunicare con il server.
 */
namespace Server {

  /**
   * @brief Il server web non accetta le richieste da utenti non loggati. Il login viene mantenuto tramite Cookies. Per questo motivo è utile averne una gestione sensata.
   */
    class Cookie {
    public:
        Cookie();
        Cookie(const std::string& str_cookie);

        std::string get_name() const { return name; } /**< Nome del cookie. */
        std::string string() const; /**< Restituisce il contenuto del cookie sotto forma di stringa. */
        class YesNo {
        public:
            explicit YesNo(bool yn) : yesno(yn) {}
                std::string operator()() const {
                    return yesno ? "Yes" : "No";
                }
                friend std::ostream &operator<<(std::ostream &strm, const YesNo &yn) {
                    strm << yn();
                    return strm;
                }
        private:
            bool yesno;
        };

	/**
	 * Utilizzato solo per il debug.
	 */
	friend std::ostream& operator<<(std::ostream &strm, const Cookie &cook)        {
            strm << "Cookie: '" << cook.name << "' (secure: " << YesNo(cook.secure) << ", tail: "
                << YesNo(cook.tail) << ") for domain: '" << cook.domain << "', "
                << "path: '" << cook.path << "'.\n";
            strm << "Value: '" << cook.value << "'.\n";
            strm << "Expires: '" << ctime(&cook.expires) << "'.\n";
            return strm;
        }
    private:
        std::string name;
        std::string value;
        std::string domain;
        std::string path;
        time_t expires;
        bool tail;
        bool secure;
    };

    /**
     * @brief Variabile globale con i cookies che fornisce il sito. Indispensabili per l'autentica
     */
    extern std::list<Cookie> cookieList;

    /**
     * @brief Lista di Cookie a partire da una stringa ben formattata e viceversa
     */
    std::list<std::string> to_string(const std::list<Cookie>& cookieList);

    /**
     * @brief Crea una lista di cookie da una lista di stringhe.
     * @param s La lista di stringhe da cui creare il cookie.
     * @param cookieList La variabile su cui copiare i cookie.
     */
    void cookieListFromStrList(std::list<std::string>& s, std::list<Cookie>& cookieList);

    /**
     * @brief Prende la foto dall'url selezionato (non serve autentica). La salva con il nome destinazione nella cartella indicata il local_settings
     * @param url Url completo da cui scaricare la foto.
     * @param destinazione Nella maggior parte dei casi è il pk del player come stringa.
     * @return Il nome del file senza percorso sui cui è stato salvato. E.g. "5.jpg", E.g. "noProfile.jpg" se la foto non esiste.
     */
    std::string salvaFoto(std::string  url, std::string  destinazione);

    /**
     * @brief Prende il csrf_token di django da una pagina fatta in un certo modo.
     * @param s Stringstream che contiene il contenuto della pagina.
     * @return Ritorna il CSRF token in forma di stringa.
     */
    std::string estrai_csrf(std::stringstream& s);

    /**
     * @brief Prima richiede la pagina di login, scarica il csrf_token, si logga e prende il cookie e lo salva in cookieList.
     * @param cookieList Il cookie su cui salvare le informazioni. Normalmente sarà la variabile globale Server::cookieList.
     */
    void login(std::list<Cookie>& cookieList);


    /**
     * @brief Fa una get request alla pagina indicata nel parametro url. Printa la pagina ottenuta sullo stringstream out e fa la richiesta usando la cookieList data come parametro.
     * @param out Dove salvare il contenuto della GET request.
     * @param url L'url a cui fare la richiesta.
     * @param cookieList I cookie da utilizzare per poter fare la richiesta.
     */
    void GET_pagina(std::stringstream& out, std::string url, std::list<Cookie> &cookieList);

    /**
     * @brief POST HTML con form indicato in formPart, con autentica via cookie. Questa funzione controlla la risposta del server e lancia eccezioni se qualcosa non va come previsto.
     * @param out Dove salvare la risposta.
     * @param url Url a cui fare la richiesta.
     * @param cookieList Normalmente sarà la variabile globale Server::cookieList.
     * @param formPart Il form completo da postare.
     */
    void POST_pagina(std::stringstream& out, std::string url, std::list<Cookie>& cookieList, curlpp::Forms& formPart);

    /**
     * @brief Serve a fare una richiesta completa POST al server.
     * Fa una prima richiesta GET utilizzando i cookies. Legge la pagina ed estrae il csrf_token dal form.
     * Poi aggiunge al parametro form il csrf corretto e infine esegue una POST request con tutti i dati. Salva la risposta su risposta.
     * @param risposta Stringstream su cui salvare la risposta della POST request.
     * @param url Url a cui fare la richiesta.
     * @param cookieList Nella maggior parte dei casi sarà la variabile globale Server::cookieList.
     * @param form Il form da utilizzare nella POST request senza il csrf, che verrà preso da solo.
     */
    void fill_form(std::stringstream& risposta, const std::string url, std::list<Cookie>& cookieList, curlpp::Forms& form);

    /**
     * @brief GET request senza autentica.
     * @param s Stringstream su cui salvare la risposta.
     * @param url Url a cui fare la richiesta.
     */
    void prendiPagina(std::stringstream& s, std::string url);

    void jsonFromStr(Json::Value&, std::string); /**< Json da std::string */
    void jsonFromStr(Json::Value&, std::stringstream); /**< Json da std::stringstream */

    /**
     * @brief Decodifica il json dato dal web server per avere informazioni su un giocatore. Salva anche la foto in locale.
     * @param barcode Il barcode del giocatore di cui si vuole sapere.
     * @return Il giocatore richiesto, oppure Giocatore::nessuno se non è stato trovato.
     */
    Giocatore richiediPlayerViaBarcode(std::string  barcode);

    /**
     * @brief Chiede al server se ci sono partite in corso e in caso affermativo si salva le informazioni di tale partita.
     * @param c Collegio di cui richiedere se ci sono partite in corso.
     * @param p Partita su cui salvare le informazioni.
     */
    bool richiediPartiteInCorso(Partita::Collegio c, Partita& p);


    void mandaRichiestaServer(std::string url, curlpp::Forms& formParts); /**< Richiesta POST generica con form. Lancia eccezioni se succede qualcosa di storto. */
    void mandaRichiestaServer(Evento *&ev); /**< Fa una richiesta POST utilizzando il form fornito dalla classe derivata di ev. */
    void mandaGoalServer(std::string  tipoGoal, unsigned int  pkPartita, std::string  squadra); /**< Autoesplicativo */
    void mandaSwapServer(unsigned int  pkPart, std::string  squadra); /**< Autoesplicativo */
    void mandaAnnullaServer(unsigned int  pkPart, std::string  squadra); /**< Autoesplicativo */
    void mandaFineServer(unsigned int  pkPartita); /**< Autoesplicativo */
    void mandaInizioServer(Partita& p); /**< Autoesplicativo */

    std::vector<std::string>& split_cookie_str(const std::string &str, std::vector<std::string> &in);
    std::vector<std::string> splitCookieStr(const std::string &str);
    class serverError : public std::exception {
    };

    class erroreServer : public serverError {
    public: virtual const char* what() const throw() { return "Errore nella richiesta al server. Il server è offline?"; }
    };

    /**
     * @brief Questa non dovrebbe mai succedere perché sappiamo esattamente in che modo si comunicano i due, ma nel dubbio è stata creata.
     */
    class badJsonDecode : public std::exception {
    public:
        badJsonDecode(std::string s) { decoded = s; }
        virtual const char* what() const throw() { std::string s("Errore nella decodifica del json: "); s += decoded; return s.c_str(); }
    private:
        std::string decoded;
    };

    /**
     * @brief La causa più probabile di questo errore è un bad_login, per cui quando viene lanciata si ritenta subito un login.
     */
    class permissionDenied : public serverError {
    public: virtual const char* what() const throw() { return "Server error (403): Permission denied"; }
        static const std::string error_message;
    };
    class erroreRichiesta : public serverError {
    public: virtual const char* what() const throw() { return "Il server ha risposto in modo inaspettato."; }
    };

    /**
     * @brief Eccezione lanciata quando sul server non viene trovato il giocatore.
     */
    class playerNotFound : public std::exception {
    public: virtual const char* what() const throw() { return "Giocatore non trovato!"; }
    };

}


#endif // SERVERREQUESTS_H
