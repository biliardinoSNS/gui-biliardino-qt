#include "local_settings.h"


std::string LOCAL::ROOT_SITE;
std::string LOCAL::BASE_URL_REQUEST;
std::string LOCAL::COLLEGIO;
std::string LOCAL::PERCORSO_FOTO;
std::string LOCAL::PERCORSO_AUDIO;
int LOCAL::GOAL_VITTORIA;
bool LOCAL::supergoal;
std::string LOCAL::username;
std::string LOCAL::password;



void LOCAL::load_settings(){
    std::ifstream file_config;
    std::string path_config = get_env_or_default("GUI_BILIARDINO_CONFIG_PATH", "/etc/biliardino/config.json");

    file_config.open(path_config);

    std::string json((std::istreambuf_iterator<char>(file_config)),
                     std::istreambuf_iterator<char>());
    
    Json::Reader reader;
    Json::Value root;
    bool parsingSuccess = reader.parse(json.c_str(), root);
    if (!parsingSuccess){
        std::cerr << "Bad config file. Call the administrator" << std::endl;
        throw "Bad config";
    }
    LOCAL::ROOT_SITE = root.get("ROOT_SITE", "127.0.0.1:8000").asString();
    LOCAL::BASE_URL_REQUEST = LOCAL::ROOT_SITE + root.get("BASE_URL_REQUEST", "/statistiche/interface/").asString();
    LOCAL::COLLEGIO = root.get("COLLEGIO", "N").asString();
    LOCAL::GOAL_VITTORIA = root.get("GOAL_VITTORIA", 10).asInt();
    LOCAL::PERCORSO_FOTO = root.get("PERCORSO_FOTO", "/home/orsobruno96/git/biliardino/GUI_Biliardino/media/").asString();
    LOCAL::PERCORSO_AUDIO = root.get("PERCORSO_AUDIO", "/home/orsobruno96/git/gui-biliardino-qt/media/").asString();
    int appo1 = root.get("supergoal", 0).asInt();
    if (appo1 == 0)
      LOCAL::supergoal = false;
    else
      LOCAL::supergoal = true;
    LOCAL::username = root.get("username", "").asString();
    LOCAL::password = root.get("password", "").asString();
}

