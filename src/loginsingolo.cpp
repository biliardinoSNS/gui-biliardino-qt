#include "loginsingolo.h"
#include "ui_loginsingolo.h"

LoginSingolo::LoginSingolo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginSingolo)
{
    ui->setupUi(this);
    connect(this, SIGNAL(sigGiocatore(Giocatore&)), parentWidget(), SLOT(prendiGiocatore(Giocatore&)));
}

LoginSingolo::~LoginSingolo(){
    delete ui;
}

void LoginSingolo::on_btnAnnulla_clicked(){
    close();
}

void LoginSingolo::on_btnOk_clicked(){
    Giocatore g;
    try {
        g = Server::richiediPlayerViaBarcode(ui->lineEdit->text().toStdString());
        if (g == Giocatore::nessuno) throw Server::erroreRichiesta();
    }
    catch (Server::erroreServer& e){
        std::cerr << e.what() << std::endl;
        ui->label->setText(QString("Errore nel server. Riprova"));
        return;
    }
    catch (Server::erroreRichiesta& e){
        std::cerr << e.what() << std::endl;
        ui->label->setText("Not found");
        return;
    }
    emit sigGiocatore(g);
    close();
}
