#include <iostream>
#include "utils.h"




int main (int argc, char* argv[]){
  using namespace std;

  if (argc == 2)  {
    // apply XOR
    string encrypt = decode(argv[1]);
    // display encrypted string
    printf("decode(\"");
    for (size_t i = 0; i < encrypt.length(); i++)
      printf("\\x%02x", encrypt[i] & 0xFF);
    printf("\");");
    cout << "Stringa normale: " << encrypt.c_str() << endl;
  }
  else {
    cout << "usage: encrypt string" << endl;
  }
  
  return 0;
}
