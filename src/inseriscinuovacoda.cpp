#include "inseriscinuovacoda.h"
#include "ui_inseriscinuovacoda.h"

InserisciNuovaCoda::InserisciNuovaCoda(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InserisciNuovaCoda)
{
    ui->setupUi(this);

    connect(this, SIGNAL(sigAddCoda(Giocatore &, Giocatore &)), parentWidget(), SLOT(addCoda(Giocatore &, Giocatore &)));
    atk = Giocatore::nessuno;
    def = Giocatore::nessuno;
}

InserisciNuovaCoda::~InserisciNuovaCoda(){
    atk = Giocatore::nessuno;
    def = Giocatore::nessuno;
    delete ui;
}

void InserisciNuovaCoda::checkAtk(){
    if (checkPlayer(atk, ui->labelCheckAtk, ui->testoAtk)) {
        if (def == Giocatore::nessuno)
            ui->testoDef->setFocus();
        else
            ui->btnOk->click();
    }
    else {
        ui->testoAtk->clear();
        ui->testoAtk->setFocus();
    }
}

void InserisciNuovaCoda::checkDef(){
    if (checkPlayer(def, ui->labelCheckDef, ui->testoDef)) {
        if (atk == Giocatore::nessuno)
            ui->testoAtk->setFocus();
        else
            ui->btnOk->click();
    }
    else {
        ui->testoDef->clear();
        ui->testoDef->setFocus();
    }
}

void InserisciNuovaCoda::on_btnCheckAtk_clicked(){
    checkAtk();
}

void InserisciNuovaCoda::on_btnCheckDef_clicked(){
    checkDef();
}

void InserisciNuovaCoda::on_btnAnnulla_clicked(){
    close();
}

void InserisciNuovaCoda::on_btnOk_clicked(){
    if (atk == Giocatore::nessuno || def == Giocatore::nessuno){
        ui->labelMsg->setText(QString("Devi registrare entrambe i giocatori!")); return;
    }
    if (atk == def){
        ui->labelMsg->setText(QString("Non puoi giocare da solo.")); return;
    }
    emit sigAddCoda(atk, def);
    close();
}

void InserisciNuovaCoda::on_testoAtk_textChanged(const QString &arg1) {
    if (arg1.size() == InserisciNuovaCoda::BARCODE_LENGHT)
        checkAtk();
}

void InserisciNuovaCoda::on_testoDef_textChanged(const QString &arg1) {
    if (arg1.size() == InserisciNuovaCoda::BARCODE_LENGHT)
        checkDef();
}

bool InserisciNuovaCoda::checkPlayer(Giocatore &g , QLabel *testo, QLineEdit *barc){
    Giocatore giocatore = Giocatore::nessuno;
    try{
        giocatore = Server::richiediPlayerViaBarcode(barc->text().toStdString());
    }
    catch (Server::erroreServer& e){
        testo->setText(QString::fromStdString("Errore nel server, riprova!"));
        std::cerr << e.what() << std::endl;
        return false;
    }
    catch (Server::permissionDenied& e){
        Server::login(Server::cookieList);
        testo->setText(QString::fromStdString("Errore nel server, riprova!"));
        return false;
    }
    catch (curlpp::RuntimeError& e){
        testo->setText(QString::fromStdString("Errore nel server, non risponde"));
        return false;
    }

    if (giocatore.get_nome() == "noPlayer" || giocatore == Giocatore::nessuno){
        testo->setText(QString::fromStdString("Giocatore non trovato!"));
        giocatore = Giocatore::nessuno;
        return false;
    }
    else{
        testo->setText(QString::fromStdString(giocatore.get_nome()));
        g = giocatore;
        return true;
    }
}
