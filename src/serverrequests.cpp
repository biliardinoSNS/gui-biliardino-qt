#include "serverrequests.h"



std::list<Server::Cookie> Server::cookieList;
const std::string Server::permissionDenied::error_message = "<h1>403 Forbidden</h1>";



void Server::GET_pagina(std::stringstream& out, std::string url, std::list<Cookie>& cookieList){
    curlpp::Cleanup cleanup;
    curlpp::Easy req;

    req.setOpt(new curlpp::options::Url(url));
    for (auto it = cookieList.begin(); it != cookieList.end(); ++it){
        req.setOpt(new curlpp::options::CookieList(it->string()));
    }
    req.setOpt(new curlpp::options::WriteStream(&out));
    req.perform();
    std::list<std::string> cookie;
    std::list<Cookie> cookieOut;
    curlpp::infos::CookieList::get(req, cookie);
    if (out.str() == permissionDenied::error_message){
        throw permissionDenied();
    }

    for (auto it = cookie.begin(); it != cookie.end(); it++){
        cookieOut.push_back(Cookie(*it));
    }
    cookieList = cookieOut;
    return;
}


void Server::POST_pagina(std::stringstream& out, std::string url, std::list<Cookie>& cookieList, curlpp::Forms& formPart){
    curlpp::Cleanup cleanup;
    curlpp::Easy reqPOST;

    reqPOST.setOpt(new curlpp::options::Url(url));
    reqPOST.setOpt(new curlpp::options::HttpPost(formPart));
    reqPOST.setOpt(new curlpp::options::WriteStream(&out));

    std::list<std::string> appo;

    for (auto it = cookieList.begin(); it != cookieList.end(); it++){
        reqPOST.setOpt(new curlpp::options::CookieList(it->string()));
        appo.push_back(it->string());
    }
    reqPOST.perform();
    if (out.str() == permissionDenied::error_message){
        throw permissionDenied();
    }
    curlpp::infos::CookieList::get(reqPOST, appo);
    cookieListFromStrList(appo, cookieList);
}


void Server::login(std::list<Cookie>& cookieList){
    curlpp::Cleanup myCleanup;
    std::stringstream loginPage, result;
    std::string url(LOCAL::BASE_URL_REQUEST + "login/");

    cookieList.clear();
    Cookie appo1(".google.com\tTRUE\t/\tFALSE\t2147483647\tLSID\tI like you GOOGLE");
    cookieList.push_back(appo1);
    GET_pagina(loginPage, url, cookieList);

    std::string csrf_token = Server::estrai_csrf(loginPage);
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("username", LOCAL::username));
    formParts.push_back(new curlpp::FormParts::Content("password", LOCAL::password));
    formParts.push_back(new curlpp::FormParts::Content("csrfmiddlewaretoken", csrf_token));
    POST_pagina(result, url, cookieList, formParts);
}




void Server::prendiPagina(std::stringstream & out, std::string url){
    try{
        curlpp::Cleanup cleanup;
        curlpp::Easy req;
        req.setOpt<curlpp::options::Url>(url);
        curlpp::options::WriteStream ws(&out);
        req.setOpt(ws);
        req.perform();
    }
    catch (curlpp::RuntimeError& e){
        std::cerr << e.what() << std::endl;
        throw Server::erroreServer();
    }
    catch (curlpp::LogicError& e){
        std::cerr << e.what() << std::endl;
        throw Server::erroreServer();
    }
}



std::string Server::salvaFoto(std::string url, std::string destinazione){
    try {
        std::ofstream fileOut;
        std::string percorso = Giocatore::PERCORSO_FOTO + destinazione + ".jpg";
        fileOut.open(percorso);
        std::stringstream out;
        Server::prendiPagina(out, url);
        fileOut << out.rdbuf();
        fileOut.close();
        return destinazione + ".jpg";
    }
    catch (Server::erroreServer& e){
        std::cerr << e.what() << std::endl;
        return "noProfile.jpg";
    }
}



Giocatore Server::richiediPlayerViaBarcode(std::string barcode){
    std::stringstream out;
    try {
        std::string url(LOCAL::BASE_URL_REQUEST + "player/" + barcode + "/");
        if (cookieList.empty()){
            login(cookieList);
        }

        Server::GET_pagina(out, url, cookieList);
        Json::Value root;
        Server::jsonFromStr(root, out.str());
        Giocatore ret(root, barcode);
        std::string percorsoFoto = Server::salvaFoto(LOCAL::ROOT_SITE + ret.get_urlFoto(), std::to_string((int) ret.get_pk()));
        ret.set_foto(percorsoFoto);
        return ret;
    }
    catch (Server::erroreRichiesta& e){
        std::cerr << e.what() << std::endl;
        return Giocatore::nessuno;
    }
    catch (Server::badJsonDecode& e){
        std::cerr << e.what() << std::endl;
        return Giocatore::nessuno;
    }

}



void Server::fill_form(std::stringstream& risposta, const std::string url, std::list<Cookie>& cookieList, curlpp::Forms& form){
    std::stringstream get;
    GET_pagina(get, url, cookieList);
    std::string csrf_token = estrai_csrf(get);
    form.push_back(new curlpp::FormParts::Content("csrfmiddlewaretoken", csrf_token));
    POST_pagina(risposta, url, cookieList, form);
}


void Server::mandaInizioServer(Partita &p){
    std::string url(LOCAL::BASE_URL_REQUEST + "set/");
    std::stringstream risposta;

    auto g = p.get_giocatori();
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("collegio", col_breve(p.collegio)));
    formParts.push_back(new curlpp::FormParts::Content("tipo", "I"));
    for (auto it = g.begin(); it != g.end(); it++){
        formParts.push_back(new curlpp::FormParts::Content(it->first.breve(), it->second.get_barcode()));
    }
    fill_form(risposta, url, cookieList, formParts);
    int ris;
    risposta >> ris;
    if  (risposta.fail()){
        throw serverError();
    }
    p.set_pk(ris);
}


void Server::mandaRichiestaServer(std::string url, curlpp::Forms &formParts){
    std::stringstream appo;
    fill_form(appo, url, cookieList, formParts);
    std::string risposta = appo.str();
    if (risposta == "error"){
        throw Server::erroreRichiesta();
    }
    else if (risposta == permissionDenied::error_message){
        throw permissionDenied();
    }
}

void Server::mandaRichiestaServer(Evento *&ev){
    curlpp::Forms form = ev->form();
    std::stringstream risposta;
    fill_form(risposta, ev->url(), cookieList, form);
    std::string appo = risposta.str();
    if (appo == "error"){
        throw erroreRichiesta();
    }
    else if (appo == permissionDenied::error_message){
        throw permissionDenied();
    }
    ev->gestisciRisposta(risposta);
}


void Server::mandaSwapServer(unsigned int pkPart, std::string squadra){
    std::string url(LOCAL::BASE_URL_REQUEST + "set/");
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("idPartita", std::to_string(pkPart)));
    formParts.push_back(new curlpp::FormParts::Content("squadra", squadra));
    formParts.push_back(new curlpp::FormParts::Content("tipo", "S"));
    return mandaRichiestaServer(url, formParts);
}



void Server::mandaGoalServer(std::string tipoGoal, unsigned int pkPartita, std::string squadra){
    std::string url(LOCAL::BASE_URL_REQUEST + "set/");
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("idPartita", std::to_string(pkPartita)));
    formParts.push_back(new curlpp::FormParts::Content("squadra", squadra));
    formParts.push_back(new curlpp::FormParts::Content("tipo", tipoGoal));
    return mandaRichiestaServer(url, formParts);
}


void Server::mandaFineServer(unsigned int pkPartita){
    std::string url(LOCAL::BASE_URL_REQUEST + "set/");
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("idPartita", std::to_string(pkPartita)));
    formParts.push_back(new curlpp::FormParts::Content("tipo", "F"));
    return mandaRichiestaServer(url, formParts);
}

void Server::mandaAnnullaServer(unsigned int pkPartita, std::string squadra){
    std::string url(LOCAL::BASE_URL_REQUEST + "set/");
    curlpp::Forms formParts;
    formParts.push_back(new curlpp::FormParts::Content("idPartita", std::to_string(pkPartita)));
    formParts.push_back(new curlpp::FormParts::Content("tipo", "A"));
    formParts.push_back(new curlpp::FormParts::Content("squadra", squadra));
    return mandaRichiestaServer(url, formParts);
}




bool Server::richiediPartiteInCorso(Partita::Collegio c, Partita &p){
    std::stringstream out;
    p = Partita();

    try {
        std::string url(LOCAL::BASE_URL_REQUEST + "part/" + col_breve(c) + "/");

        if (cookieList.empty()){
            login(cookieList);
        }
        GET_pagina(out, url, cookieList);
        Json::Value root;
        Server::jsonFromStr(root, out.str());
        int prova = fromStr(root.get("pkPart", "-1").asString());
        if (prova == -1){
            p = Partita();
            return false;
        }
        Partita appo(root);
        p = appo;
        return true;
    }
    catch (Server::erroreServer& e){
        std::cerr << e.what() << std::endl;
        return false;
    }
    catch (Server::badJsonDecode& e){
        std::cerr << e.what() << std::endl;
        return false;
    }
    return false;
}









std::vector<std::string>& Server::split_cookie_str(const std::string &str, std::vector<std::string> &in){
    std::string part;

    std::istringstream strm(str);
    while (getline(strm, part, '\t'))
        in.push_back(part);

    return in;
}

std::vector<std::string> Server::splitCookieStr(const std::string &str){
    std::vector<std::string> split;
    split_cookie_str(str, split);
    return split;
}


Server::Cookie::Cookie(const std::string &str_cookie){
    std::vector<std::string> vC = splitCookieStr(str_cookie);

    domain = vC[0];
    tail = vC[1] == "TRUE";
    path = vC[2];
    secure = vC[3] == "TRUE";
    expires = fromStr(vC[4]);
    name = vC[5];
    value = vC[6];
}

std::string Server::Cookie::string() const {
    std::string ret(domain);
    std::string appo;
    if (tail == true){
        appo = "TRUE";
    }
    else    {
        appo = "FALSE";
    }
    ret += ("\t" + appo);
    ret += ("\t" + path);
    if (secure == true){
        appo = "TRUE";
    }
    else    {
        appo = "FALSE";
    }
    ret += ("\t" + appo);
    ret += ("\t" + std::to_string(expires));
    ret += ("\t" + name);
    ret += ("\t" + value);
    return ret;
}


Server::Cookie::Cookie(){
    domain = "";
    tail = false;
    secure = false;
    path = "";
    expires = 0;
    name = "";
    value = "";
}


std::string Server::estrai_csrf(std::stringstream &s){
    char csrf[64];
    std::stringstream ss;
    ss << s.str() << std::endl; /* è insensato ma senza non funziona */
    try {
        std::sscanf(s.str().c_str(), "<input type='hidden' name='csrfmiddlewaretoken' value='%64s' />", csrf);
    }
    catch (std::exception& e){
        std::cerr << e.what() << std::endl;
        throw Server::erroreServer();
    }
    std::string ret(csrf);
    return ret;
}



void Server::jsonFromStr(Json::Value &root, std::string s){
    Json::Reader reader;
    bool parsingSuccessful = reader.parse( s.c_str(), root );     //parse process
    if ( !parsingSuccessful )    {
        throw Server::badJsonDecode(reader.getFormattedErrorMessages());
    }
}

void Server::jsonFromStr(Json::Value& root, std::stringstream s){
    return Server::jsonFromStr(root, s.str());
}


std::list<std::string> Server::to_string(const std::list<Cookie>& cookieList) {
    std::list<std::string> ret;
    for (auto it = cookieList.begin(); it != cookieList.end(); it++){
        ret.push_back(it->string());
    }
    return ret;
}

void Server::cookieListFromStrList(std::list<std::string>& s, std::list<Cookie>& cookieList){
    cookieList.clear();
    for (auto it = s.begin(); it != s.end(); it++){
        cookieList.push_back(Cookie(*it));
    }
}
