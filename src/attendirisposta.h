
/**
 * @file attendirisposta.h
 * @brief Threads per attendere la risposta del server senza congelare la GUI. Annullano gli eventi in caso il server risponda male. Qui viene definita la classe AttendiRisposta
 */


#ifndef ATTENDIRISPOSTA_H
#define ATTENDIRISPOSTA_H

#include <QObject>
#include <QThread>


#include "serverrequests.h"
#include "evento.h"
#include "cambiorating.h"


/**
 * @brief classe che gestisce l'attesa di una risposta dal server usando le API scritte in serverrequests.h
 */
class AttendiRisposta : public QThread
{
    Q_OBJECT
public:
    /**
     * @brief Costruttore
     * @param ev Questo parametro non è mai di tipo Evento ma sempre di una sua classe derivata. 
     */
    explicit AttendiRisposta(QObject *parent = 0, Evento* ev = nullptr);
    ~AttendiRisposta();

    /**
     * @brief Fa partire il thread e gestisce le risposte. Viene fatto tutto qui. 
     * Ampia gestione degli errori. L'unico non spiegato nel dettaglio è un catch (...) che restituisce un "Errore non previsto!". Consiglio di toglierlo in fase di debug per capire dove esplode
     */
    void run(); 
    void stop() { quit(); exit(); } /**< Ferma il thread */

signals:
    void sigError(Evento* evento); /**< Comunica alla MainWindow che l'ultimo evento è stato annullato perché il server ha risposto in modo inaspettato */
    void sigLogin(); /**< Richiede alla MainWindow che venga rifatto il login a causa di un error 403 */
public slots:

private:
    Evento* evento;
};

#endif // ATTENDIRISPOSTA_H
