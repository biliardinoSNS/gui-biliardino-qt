/**
 * @file loginsingolo.h
 * @brief Mini finestra di dialogo chiamata da CambiaSigma per permettere solo agli utenti loggati di cambiare la calibrazione. I metodi sono quasi copiati da inseriscinuovacoda.h
 */


#ifndef LOGINSINGOLO_H
#define LOGINSINGOLO_H

#include <QDialog>
#include "serverrequests.h"


namespace Ui {
class LoginSingolo;
}

class LoginSingolo : public QDialog
{
    Q_OBJECT

public:
    explicit LoginSingolo(QWidget *parent = 0);
    ~LoginSingolo();

signals:
    void sigGiocatore(Giocatore&);

private slots:
    void on_btnAnnulla_clicked();
    void on_btnOk_clicked();

private:
    Ui::LoginSingolo *ui;
};

#endif // LOGINSINGOLO_H
