/**
 * @file plot.h
 * @brief Gestione dei grafici delle porte analogiche di Arduino
 */

#ifndef PLOT_H
#define PLOT_H

#include <QDialog>
#include <QTimer>
#include <QCloseEvent>
#include <QTime>


#include "arduino.h"
#include "../lib/qcustomplot/qcustomplot.h"

namespace Ui {
class Plot;
}


/**
 * @brief Dialog con il grafico in tempo reale delle porte di Arduino.
 * Quando viene creata questa finestra dalla MainWindow viene fermato il thread che legge dalla porta seriale e viene ricreato quando viene chiusa, altrimenti i due si rubano i messaggi a vicenda.
 */
class Plot : public QDialog
{
    Q_OBJECT

public:
    explicit Plot(bool sg, Arduino::Porte porta, Arduino* ard, QWidget *parent = 0);
    ~Plot();
    void closeEvent(QCloseEvent* event);


    
signals:
    /**
     * @brief Chiede alla MainWindow di chiedere ad Arduino di riprendere il funzionamento normale.
     */
    void sigStop();

public slots:
  void realtimeDataSlot(); /**< Per aggiornare ogni tot tempo */
    
private:
    Ui::Plot *ui;
    QTimer dataTimer;
    bool supergoal;
    Arduino::Porte porta; /**< La porta di Arduino da leggere */
    Arduino* arduino;
    QTime loraesatta;
    double lastPointKey;
};

#endif // PLOT_H
