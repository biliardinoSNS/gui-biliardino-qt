#include "partita.h"


unsigned int Partita::GOAL_VITTORIA = LOCAL::GOAL_VITTORIA;
Partita::Collegio Partita::collegio = Partita::Collegio::Timpano; //coll_fromStdStr(LOCAL::COLLEGIO); non si può mettere qui, quando viene eseguito questo le local_settings non sono ancora state caricate



Partita::Partita()
{
    Partita::Posizione AB(Giocatore::Ruolo::Attacco, Partita::Squadra::Blu),\
            AR(Giocatore::Ruolo::Attacco, Partita::Squadra::Red),\
            DB(Giocatore::Ruolo::Difesa, Partita::Squadra::Blu),\
            DR(Giocatore::Ruolo::Difesa, Partita::Squadra::Red);
    Partita::Posizione nomi[] = { AB, AR, DB, DR };
    for (unsigned int i = 0; i < 4; i++) {
        giocatori[nomi[i]] = Giocatore::nessuno;
    }
    punteggi[Partita::Squadra::Red] = 0;
    punteggi[Partita::Squadra::Blu] = 0;
    pk = -1;
    GOAL_VITTORIA = LOCAL::GOAL_VITTORIA;
    vantaggi = false;
}


/*
Partita::Partita(){
    Ruolo ruoli[] = { Ruolo::Attacco, Ruolo::Difesa };
    Squadra squadre[] = { Squadra::Blu, Squadra::Red };
    for (int i = 0; i < 2; i++){
        for (int j = 0; j < 2; j++){
            Posizione p(ruoli[i], squadre[j]);
            giocatori[p] = Giocatore::nessuno;
        }
    }
    punteggi[Squadra::Blu] = punteggi[Squadra::Red] = 0;
}
*/


Partita::Partita(Giocatore ab, Giocatore ar, Giocatore db, Giocatore dr, unsigned int pb, unsigned int pr){
    Partita::Posizione AB(Giocatore::Ruolo::Attacco, Partita::Squadra::Blu),\
            AR(Giocatore::Ruolo::Attacco, Partita::Squadra::Red),\
            DB(Giocatore::Ruolo::Difesa, Partita::Squadra::Blu),\
            DR(Giocatore::Ruolo::Difesa, Partita::Squadra::Red);
    giocatori[AR] = ar;
    giocatori[AB] = ab;
    giocatori[DB] = db;
    giocatori[DR] = dr;
    punteggi[Partita::Squadra::Red] = pr;
    punteggi[Partita::Squadra::Red] = pb;
    vantaggi = false;
}


Partita::Collegio coll_fromStdStr(const std::string s){
    if (s == "T") return Partita::Collegio::Timpano;
    if (s == "C") return Partita::Collegio::Carducci;
    if (s == "F") return Partita::Collegio::Fermi;
    if (s == "E") return Partita::Collegio::Faedo;
    else throw "COLLEGIO INESISTENTE";
}

// Modifica i punteggi, gestendo eventuali effetti collaterali
void Partita::edit_punteggi(Squadra s, int pt, bool coda) {
    punteggi[s] += pt;
    // controlla se iniziano i vantaggi
    if (!coda && punteggi[Partita::Squadra::Red] == Partita::GOAL_VITTORIA - 1 && punteggi[Partita::Squadra::Blu] == Partita::GOAL_VITTORIA - 1)
        vantaggi = true;
}

// Restituisce true sse la parita deve finire
bool Partita::fine() {
    // verifica sui punteggi: se sono minori dei GOAL_VITTORIA la partita non è finita
    if (punteggi[Partita::Squadra::Red] < Partita::GOAL_VITTORIA && punteggi[Partita::Squadra::Blu] < Partita::GOAL_VITTORIA)
        return false;
    // se siamo ai vantaggi e la differenza dei punteggi è < 2 la partita non è finita
    if (vantaggi && abs(punteggi[Partita::Squadra::Red] - punteggi[Partita::Squadra::Blu]) < 2)
        return false;
    // Questo dovrebbe permettere partite autistiche (o errori tipo coda sul software che in realtà non esiste) chiedendo al più una volta di terminare la partita (se non ho sbagliato la logica elementare)
    if (!vantaggi && (punteggi[Partita::Squadra::Red] > Partita::GOAL_VITTORIA || punteggi[Partita::Squadra::Blu] > Partita::GOAL_VITTORIA ||
                      (punteggi[Partita::Squadra::Red] == Partita::GOAL_VITTORIA && punteggi[Partita::Squadra::Blu] == Partita::GOAL_VITTORIA)))
        return false;
    // Qui almeno un punteggio è >= GOAL_VITTORIA e (i vantaggi non sono mai iniziati o differenza dei punteggi >= 2) e l'autismo è soddisfatto
    return true;
}


std::string s_breve(const Partita::Squadra &s){
    if (s == Partita::Squadra::Red) return "R"; else return "B";
}



bool Partita::controllaNessuno() const {
    for (auto it = giocatori.begin(); it != giocatori.end(); it++){
        if (it->second == Giocatore::nessuno){
            return true;
        }
    }
    return false;
}


bool Partita::controllaPlayerUguali() const {
    for (auto it = giocatori.begin(); it != giocatori.end(); it++){
        auto kt = it;
        kt++;
        for (auto jt = kt; jt != giocatori.end(); jt++){
            if (it->second == jt->second){
                return true;
            }
        }
    }
    return false;
}


std::string col_breve(const Partita::Collegio c){
    switch (c) {
    case Partita::Collegio::Carducci: return "C"; break;
    case Partita::Collegio::Faedo: return "E"; break;
    case Partita::Collegio::Fermi: return "F"; break;
    case Partita::Collegio::Timpano: return "T"; break;
    default: throw "Cusu"; break;
    }

}


Partita::Partita(Json::Value v){
    try {
        Giocatore appo[4];
        Partita::Posizione AB(Giocatore::Ruolo::Attacco, Partita::Squadra::Blu),\
                AR(Giocatore::Ruolo::Attacco, Partita::Squadra::Red),\
                DB(Giocatore::Ruolo::Difesa, Partita::Squadra::Blu),\
                DR(Giocatore::Ruolo::Difesa, Partita::Squadra::Red);
        Partita::Posizione appos[4] = { AB, AR, DB, DR };
        for (unsigned int i = 0; i < 4; i++){
            std::string n = v.get("nome" + appos[i].breve(), "cusu").asString();
            std::string b = v.get("barcode" + appos[i].breve(), "cusu").asString();
            std::string ur = v.get("foto" + appos[i].breve(), "cusu").asString();
            int key = fromStr(v.get("pk" + appos[i].breve(), "0").asString());
            Giocatore::Rating atk(v.get("r_atk" + appos[i].breve() + "_m", 1).asInt(), v.get("r_atk" + appos[i].breve() + "_s", 1).asInt());
            Giocatore::Rating def(v.get("r_def" + appos[i].breve() + "_m", 1).asInt(), v.get("r_def" + appos[i].breve() + "_s", 1).asInt());
            appo[i] = Giocatore(n, b, ur, std::to_string(key) + ".jpg", key, def, atk);
            giocatori[appos[i]] = appo[i];
        }
        punteggi[Partita::Squadra::Red] = fromStr(v.get("goalRed", "0").asString());
        punteggi[Partita::Squadra::Blu] = fromStr(v.get("goalBlu", "0").asString());
        vantaggi = false;
        pk = fromStr(v.get("pkPart", "0").asString());
    }
    catch (...){
        std::cerr << "Partita inizializzata male!" << std::endl;
    }

}


std::ostream& operator<<(std::ostream& os, const Partita& p){
    os << "Id partita: " << p.pk << "\tCollegio: " << col_lungo(p.collegio) <<std::endl;
    auto cusu = p.giocatori;
    for (auto it = cusu.begin(); it != cusu.end(); it++){
        os << it->first.breve() << ": " << it->second << std::endl;
    }
    return os;
}


std::string col_lungo(const Partita::Collegio c){
    switch (c) {
    case Partita::Collegio::Timpano: return "Timpano"; break;
    case Partita::Collegio::Carducci: return "Carducci"; break;
    case Partita::Collegio::Fermi: return "Fermi"; break;
    case Partita::Collegio::Faedo: return "Faedo"; break;
    default: return "";
    }
}
