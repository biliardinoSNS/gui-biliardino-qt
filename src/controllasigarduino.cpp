#include "controllasigarduino.h"


controllaSigArduino::controllaSigArduino(QObject* parent, Arduino* ard) : QThread(parent) {
    continua = true;
    arduino = ard;
    tempoUltimoComando = QTime::currentTime();
    qRegisterMetaType<Partita::EventoSquadra>("Partita::EventoSquadra");
    connect(this, SIGNAL(sigRicollega()), parent, SLOT(ricollegaArd()));
    connect(this, SIGNAL(sigEvento(Partita::EventoSquadra)), parent, SLOT(eventoArduino(Partita::EventoSquadra)));
}


void controllaSigArduino::prendiArd(Arduino* ard){
    arduino = ard;
}


/*
 * Qui c'è una soluzione ad hoc che fa abbastanza cagare per impedire che il processo venga terminato male.
 * In pratica se gli do sleep(10) lui non accetta ragioni e non si termina prima di 10 secondi. Invece fatto come l'ho fatto io
 * Lui controlla ogni secondo se gli ho chiesto di terminarsi e dopo 10 secondi prova il ricollegamento invece che ogni secondo che è inutile
 *
*/
void controllaSigArduino::run(){
    int tempo = 0;
    while (continua){
        try {
            if (arduino == nullptr) throw Arduino::noArd();
            if (tempo == 0 && arduino->esiste()){
                if (arduino->available()) {
                    std::string messaggio = arduino->readAll();
                    if (!messaggio.empty())
                        std::cout << "Messaggio: " << messaggio << " fine" << std::endl;
                    QTime adesso = QTime::currentTime();
                    if ((adesso - tempoUltimoComando) > TimeDelta::FromSeconds(1) && !messaggio.empty()){
                        gestisciMessaggio(messaggio);
                        tempoUltimoComando = adesso;
                    }
                    msleep(100);
                    tempo = 0;
                }
                else {
                    QThread::msleep(100);
                }
            }
            else throw Arduino::noArd();
        }
        catch (Arduino::noArd& e){
                if (tempo == 0){
                    std::cerr << "Arduino non disponibile. Provo a ricollegarlo." << std::endl;
                    emit sigRicollega();
                }
                sleep(1);
                tempo++;
                if (tempo >= 10){
                    tempo = 0;
                }
            }
        catch (badMessage& e){
            std::cerr << e.what() << std::endl;
        }
        catch (Arduino::erroreLettura& e){
            std::cerr << e.what() << std::endl;
        }
    }
}


void controllaSigArduino::gestisciMessaggio(std::string m){
    std::map<std::string, Partita::EventoSquadra> mappa;
    mappa["Gr"] = Partita::EventoSquadra(Partita::tipoEvento::GOAL_F, Partita::Squadra::Red); // MOLTISSIMA ATTENZIONE QUANDO SI ATTIVANO. CUSU
    mappa["Gb"] = Partita::EventoSquadra(Partita::tipoEvento::GOAL_F, Partita::Squadra::Blu);
    mappa["bbp"] = Partita::EventoSquadra(Partita::tipoEvento::GOAL_B, Partita::Squadra::Blu);
    mappa["bbm"] = Partita::EventoSquadra(Partita::tipoEvento::GOAL_ANN, Partita::Squadra::Blu);
    mappa["brp"] = Partita::EventoSquadra(Partita::tipoEvento::GOAL_B, Partita::Squadra::Red);
    mappa["brm"] = Partita::EventoSquadra(Partita::tipoEvento::GOAL_ANN, Partita::Squadra::Red);
    mappa["bsr"] = Partita::EventoSquadra(Partita::tipoEvento::SWAP, Partita::Squadra::Red);
    mappa["bsb"] = Partita::EventoSquadra(Partita::tipoEvento::SWAP, Partita::Squadra::Blu);
    mappa["GR"] = Partita::EventoSquadra(Partita::tipoEvento::SUPERGOAL, Partita::Squadra::Red);
    mappa["GB"] = Partita::EventoSquadra(Partita::tipoEvento::SUPERGOAL, Partita::Squadra::Blu);
    /* DEBUG
    Questa cosa si può ottimizzare e scrivere in modo migliore
*/

    for (auto it = mappa.begin(); it != mappa.end(); it++){
        if (m.find(it->first) != std::string::npos){
            emit sigEvento(it->second);
            return;
        }
    }
}
