/**
 * @file guida.h
 * @brief Guida in linea per l'utente. È semplicemente un QWidget che 
 * apre come homepage la guida in linea del biliardino su UZ
 */



#ifndef GUIDA_H
#define GUIDA_H

#include <QDialog>
#include <QTreeView>
#include <QStandardItemModel>

namespace Ui {
class Guida;
}

class Guida : public QDialog
{
    Q_OBJECT

public:
    explicit Guida(QWidget *parent = 0);
    ~Guida();
    QString testoDaTitolo(std::string s);


private:
    Ui::Guida *ui;
};

#endif // GUIDA_H
