/**
 * @file inseriscinuovacoda.h
 * @brief Dialog per inserire due nuove persone in coda.
 */


#ifndef INSERISCINUOVACODA_H
#define INSERISCINUOVACODA_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include "giocatore.h"
#include "serverrequests.h"



namespace Ui {
class InserisciNuovaCoda;
}

/**
 * @brief QDialog per inserire le persone.
 * Le persone non vengono inserite in coda finché qualcuno non preme il pulsante Ok. Per poterlo premere è necessario che entrambe i giocatori siano registrati e riconosciuti dal server.
 */
class InserisciNuovaCoda : public QDialog
{
    Q_OBJECT

public:
    explicit InserisciNuovaCoda(QWidget *parent = 0);
    ~InserisciNuovaCoda();

    /**
     * @brief Controlla se il giocatore richiesto esiste e in caso contrario si lamenta. Se invece esiste lo aggiunge.
     */
    bool checkPlayer(Giocatore&, QLabel*, QLineEdit*);

signals:
    /**
     * @brief Manda alla MainWindow i giocatori che si sono registrati, prima atk poi def
     */
    void sigAddCoda(Giocatore& , Giocatore &);


private slots:
    void on_btnCheckAtk_clicked();
    void on_btnCheckDef_clicked();
    void on_btnAnnulla_clicked(); /**< Chiude semplicemente la finestra */
    void on_btnOk_clicked(); /**< Controlla che tutti i giocatori siano a posto, poi manda un signal sigAddCoda alla MainWindow e chiude il dialog */
    void on_testoAtk_textChanged(const QString &arg1);  /**< Tricks vari per fare l'autofocus delle cose giuste e far perdere meno tempo all'utente */
    void on_testoDef_textChanged(const QString &arg1);  /**< Tricks vari per fare l'autofocus delle cose giuste e far perdere meno tempo all'utente */

    void checkAtk(); /**< Tricks vari per fare l'autofocus delle cose giuste e far perdere meno tempo all'utente */
    void checkDef(); /**< Tricks vari per fare l'autofocus delle cose giuste e far perdere meno tempo all'utente */


private:
    const unsigned char BARCODE_LENGHT = 16;
    Ui::InserisciNuovaCoda *ui;
    Giocatore atk; /**< Giocatore da aggiungere messo come attaccante */
    Giocatore def; /**< Giocatore da aggiungere messo come difensore */
};

#endif // INSERISCINUOVACODA_H
