#-------------------------------------------------
#
# Project created by QtCreator 2017-02-19T22:30:07
#
#-------------------------------------------------

QT       += core gui serialport printsupport webkit webkitwidgets multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUI_Biliardino
TEMPLATE = app

FLAGS += -Wall -pedantic -I/usr/include/jsoncpp

LIBS += -lcurl -lcurlpp -ljsoncpp

SOURCES += main.cpp\
        mainwindow.cpp \
    inseriscinuovacoda.cpp \
    finepartita.cpp \
    serverrequests.cpp \
    giocatore.cpp \
    partita.cpp \
    local_settings.cpp \
    utils.cpp \
    arduino.cpp \
    plot.cpp \
    cambiasigma.cpp \
    loginsingolo.cpp \
    guida.cpp \
    runguard.cpp \
    attendirisposta.cpp \
    evento.cpp \
    controllasigarduino.cpp \
    cambiorating.cpp

HEADERS  += mainwindow.h \
    inseriscinuovacoda.h \
    finepartita.h \
    serverrequests.h \
    giocatore.h \
    partita.h \
    local_settings.h \
    utils.h \
    arduino.h \
    plot.h \
    cambiasigma.h \
    loginsingolo.h \
    guida.h \
    runguard.h \
    attendirisposta.h \
    evento.h \
    controllasigarduino.h \
    cambiorating.h

FORMS    += mainwindow.ui \
    inseriscinuovacoda.ui \
    finepartita.ui \
    plot.ui \
    cambiasigma.ui \
    loginsingolo.ui \
    guida.ui \
    cambiorating.ui

# Library
HEADERS += ../lib/time_delta/time_delta.h \
    ../lib/qcustomplot/qcustomplot.h

SOURCES += ../lib/time_delta/time_delta.cpp \
    ../lib/qcustomplot/qcustomplot.cpp

CONFIG += c++14

DISTFILES +=
