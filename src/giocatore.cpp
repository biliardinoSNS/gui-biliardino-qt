#include "giocatore.h"


const Giocatore Giocatore::nessuno;
const std::string Giocatore::BASE_URL_FOTO = LOCAL::ROOT_SITE + "media/fotoUtenti/";
std::string Giocatore::PERCORSO_FOTO;


Giocatore::Giocatore(){
  pk = 0;
  nome = "Nessuno";
  barcode = "";
  foto = "noProfile.jpg";
  urlFoto = "";
  rating[Ruolo::Attacco] = Rating();
  rating[Ruolo::Difesa] = Rating();
  oldRating[Ruolo::Attacco] = Rating();
  oldRating[Ruolo::Difesa] = Rating();
}


Giocatore::Giocatore(std::string n, std::string bar, int p){
  pk = p; nome = n;
  barcode = bar;
  foto = "";
  urlFoto = "";
  rating[Ruolo::Attacco] = Rating();
  rating[Ruolo::Difesa] = Rating();
  oldRating[Ruolo::Attacco] = Rating();
  oldRating[Ruolo::Difesa] = Rating();
}


Giocatore::Giocatore(std::string n, std::string bar, std::string ur, std::string f, int p) {
  pk = p;
  nome = n;
  barcode = bar;
  urlFoto = ur;
  foto = f;
}


Giocatore::Giocatore(std::string n, std::string bar,
	  std::string ur, std::string f, int p, Rating d, Rating a) {
  pk = p;
  nome = n;
  barcode = bar;
  urlFoto = ur; foto = f;
  rating[Ruolo::Difesa] = d; rating[Ruolo::Attacco] = a;
  oldRating[Ruolo::Difesa] = d; oldRating[Ruolo::Attacco] = a;
}


Giocatore::Giocatore(const Giocatore& rhs) {
  nome = rhs.nome; foto = rhs.foto;
  pk = rhs.pk; barcode = rhs.barcode; urlFoto = rhs.urlFoto;
  rating = rhs.rating; oldRating = rhs.oldRating;
}


Giocatore::Giocatore(Json::Value val, std::string bar){
    try{
        nome = val.get("nome", "nessuno").asString();
        if (nome == "nessuno"){
            *this = Giocatore::nessuno;
            return;
        }
        std::string appo = val.get("pk", "0").asString();
        std::stringstream appo2;
        appo2 << appo;
        appo2 >> pk;
        barcode = bar;
        urlFoto = val.get("foto", "cusu").asString();
        // foto = Server::salvaFoto(urlFoto, std::to_string((int) pk));
        rating[Ruolo::Attacco] = Rating(val.get("r_atk_m", 1).asInt(), val.get("r_atk_s", 1).asInt());
        rating[Ruolo::Difesa] = Rating(val.get("r_def_m", 1).asInt(), val.get("r_def_s", 1).asInt());
        oldRating[Ruolo::Attacco] = rating[Ruolo::Attacco];
        oldRating[Ruolo::Difesa] = rating[Ruolo::Difesa];
        foto = "";
    }
    catch (...){
        *this = Giocatore::nessuno;
    }
}
