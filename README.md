[![pipeline status](https://gitlab.com/biliardinoSNS/gui-biliardino-qt/badges/master/pipeline.svg)](https://gitlab.com/biliardinoSNS/gui-biliardino-qt/commits/master)
# GUI per il Raspberry, in C++

## Istruzioni di compilazione
Ho usato QT5 per la grafica. Il modo migliore per installare tutte librerie che servono per QT è installare il pacchetto `qtcreator`.


Inoltre servono anche una serie di pacchetti che adesso elenco:
- build-essential
- cmake
- qtcreator
- libqt5serialport5-dev
- libqt5webkit5-dev
- libjsoncpp-dev 
- libcurl4-openssl-dev
- qtmultimedia5-dev
- curlpp-dev (Si [trova](https://packages.debian.org/source/sid/curlpp) solo per sid)

Per compilare è sufficiente andare nella `/` del repository e fare 

```
$ make -j8
```

Per installare nel raspberry è sufficiente un 

```
make install
```


## Per il run in debug
Bisogna settare una variabile d'ambiente per indicare dove si trova il file di configurazione. Il default è in `/etc/biliardinoGUI/config.json`, ma magari non vi garba sporcare la vostra `/etc`, per cui potete risolvere con un 
`export GUI_BILIARDINO_CONFIG_PATH=${PWD}/config.json`

## Documentazione
Per generare la documentazione è necessario aver installato i pacchetti `doxygen` e `graphviz`. Si usi semplicemente il target
```bash
make docs
```
Esiste una guida in linea sulla [GitLab page](https://biliardinosns.gitlab.io/gui-biliardino-qt/) di questo repo.


## Posti con guide interessanti per capire le librerie
- https://github.com/open-source-parsers/jsoncpp#generating-amalgamated-source-and-header
- https://techoverflow.net/2013/03/15/simple-c-http-download-using-libcurl-easy-api/
- https://raspberrypi.stackexchange.com/questions/1/how-do-i-build-a-gcc-4-7-toolchain-for-cross-compiling

## Cross compile
Compilare questa cosa per un raspberry, come vogliamo fare noi, è il male. Sto provando un po' di cose, vediamo se funziona.
- https://wiki.qt.io/RaspberryPi2EGLFS


# Arduino
Tutto il materiale da caricare su arduino è nella cartella `src/BiliArduino`. Un simpatico `Makefile` permette di compilare senza aprire ArduinoIDE ma richiede comunque che siano installati i pacchetti `arduino`, `arduino-mk`, `avrdude`. Ci sono due target fatti apposta

```
make
make upload
```

E il giuoco è fatto.
