.DEFAULT_GOAL := build
.PHONY: build clean install rebuild docs encrypter test
SHELL := /bin/bash
MAKEFLAGS=--no-print-directory

BUILD-DIR=build
SOURCE-DIR=src
GROUP=cusu
CMAKE=/usr/bin/cmake

configure:
	@if ! [[ -d "$(BUILD-DIR)" ]] ; then \
	mkdir -p $(BUILD-DIR) ; fi ;
	@if ! [[ "$$GUI_BIL_RELEASE" == "1" ]] ; then \
	echo -e "Building debug version. Clean and run with env GUI_BIL_RELEASE=1 for building release version"; \
	cd $(BUILD-DIR) && $(CMAKE) -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=. ../$(SOURCE-DIR) ; \
	else echo -e "Building release version" ; \
	cd $(BUILD-DIR) && $(CMAKE) -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/ ../$(SOURCE-DIR) ; fi ;

build: configure
	cd $(BUILD-DIR) && $(MAKE)

test: build
	@CTEST_OUTPUT_ON_FAILURE=1 $(CMAKE) --build $(BUILD-DIR) --target test

run: build
	@echo "Running GUI_Biliardino:"
	@${BUILD-DIR}/GUI_Biliardino

clean:
	-rm -rf ${BUILD-DIR}

rebuild: clean build

encrypter: build
	@echo "Running encrypter:"
	@${BUILD-DIR}/encrypter

install:
	@cd $(BUILD-DIR) && $(CMAKE) --build . --target install

docs: configure
	$(CMAKE) --build $(BUILD-DIR) --target doc
